package com.android.internal.telephony;

interface ITelephony {

  boolean endCall();

  boolean isIdle();

  void answerRingingCall();

  void silenceRinger();

  boolean showCallScreen();
}