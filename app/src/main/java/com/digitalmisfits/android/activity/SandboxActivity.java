package com.digitalmisfits.android.activity;

import android.app.ActivityManager;
import android.app.ActivityManager.RecentTaskInfo;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

import java.lang.reflect.Method;
import java.util.List;

abstract public class SandboxActivity extends FragmentActivity {

    private static final String TAG = "SandboxActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT < 16) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        hasFocus = true;

        synchronized (this) {

            if (!hasFocus) {

                final ActivityManager activity = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                    activity.moveTaskToFront(getTaskId(), ActivityManager.MOVE_TASK_WITH_HOME);
                } else {
                    List<RecentTaskInfo> recentTasks = activity.getRecentTasks(1, ActivityManager.RECENT_WITH_EXCLUDED);

                    if (!recentTasks.isEmpty()) {
                        RecentTaskInfo recentTask = recentTasks.get(0);

                        Intent restartTaskIntent = new Intent(recentTask.baseIntent);
                        if (restartTaskIntent != null) {
                            restartTaskIntent.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);

                            try {
                                startActivity(restartTaskIntent);
                            } catch (ActivityNotFoundException e) {
                                Log.w("TAG", e.getMessage(), e);
                            }
                        }
                    }
                }

                //sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
                //collapseStatusBar();
            }
        }

        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    public void onBackPressed() {
        // pass
    }

    private void collapseStatusBar() {

        Object service = getSystemService("statusbar");

        if (service == null) {
            return;
        }

        try {

            Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
            Method collapse = null;

            if (Build.VERSION.SDK_INT <= 16) {
                collapse = statusbarManager.getMethod("collapse");
            } else {
                collapse = statusbarManager.getMethod("collapsePanels");
            }

            collapse.setAccessible(true);
            collapse.invoke(service);

        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {

        if ((event.getKeyCode() == KeyEvent.KEYCODE_HOME)) {
            return true;
        } else
            return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            return true;
        } else
            return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onAttachedToWindow() {

        getWindow().setType(WindowManager.LayoutParams.TYPE_BASE_APPLICATION);
        super.onAttachedToWindow();
    }
}
