package com.digitalmisfits.android.common;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;


public class Component {

    private static final String TAG = "Component";

    public static boolean isComponentEnabled(final Context context, Class<?> cls) {

        PackageManager pm = context.getPackageManager();

        String canonicalClassName = cls.getCanonicalName();
        ComponentName componentName = new ComponentName(context.getPackageName(), canonicalClassName);

        return pm.getComponentEnabledSetting(componentName) == PackageManager.COMPONENT_ENABLED_STATE_ENABLED;
    }

    public static void enableComponentSetting(final Context context, Class<?> cls) {

        overrideComponentEnabledSetting(context, cls, PackageManager.COMPONENT_ENABLED_STATE_ENABLED);
    }

    public static void enableComponentSetting(final Context context, Class<?>[] classes) {

        for(Class<?> cls: classes) {
            enableComponentSetting(context, cls);
        }
    }

    public static void disableComponentSetting(final Context context, Class<?> cls) {
        overrideComponentEnabledSetting(context, cls, PackageManager.COMPONENT_ENABLED_STATE_DISABLED);
    }

    public static void disableComponentSetting(final Context context, Class<?>[] classes) {

        for(Class<?> cls: classes) {
            disableComponentSetting(context, cls);
        }
    }

    public static void resetComponentSetting(final Context context, Class<?> cls) {
        overrideComponentEnabledSetting(context, cls, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT);
    }

    public static void resetComponentSetting(final Context context, Class<?>[] classes) {
        for(Class<?> cls: classes) {
            overrideComponentEnabledSetting(context, cls, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT);
        }
    }

    public static void overrideComponentEnabledSetting(final Context context, Class<?> cls, int state) {

        PackageManager pm = context.getPackageManager();

        String canonicalClassName = cls.getCanonicalName();
        ComponentName componentName = new ComponentName(context.getPackageName(), canonicalClassName);

        if (pm.getComponentEnabledSetting(componentName) != state) {
            pm.setComponentEnabledSetting(componentName, state, PackageManager.DONT_KILL_APP);
        }
    }
}
