package com.digitalmisfits.android.common;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import com.digitalmisfits.android.activity.DummyLauncher;

import java.util.ArrayList;
import java.util.List;

public class Launcher {

    private static final String TAG = "Launcher";

    public static void setDefaultLauncher(Activity activity, Class<?> cls) {

        /* Toggle DummyLauncher */

        if(Component.isComponentEnabled(activity, cls)) {
            Component.enableComponentSetting(activity, DummyLauncher.class);
        } else {
            Component.enableComponentSetting(activity, cls);
        }

        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setComponent(new ComponentName("android", "com.android.internal.app.ResolverActivity"));
        intent.putExtra("origin", "com.android.internal.app.ResolverActivity");
        activity.startActivity(intent);

        Component.resetComponentSetting(activity, DummyLauncher.class);
    }

    public static boolean isDefaultLauncher(final Context context, String packageName, Class<?> cls) {

        final IntentFilter filter = new IntentFilter(Intent.ACTION_MAIN);
        filter.addCategory(Intent.CATEGORY_HOME);

        List<IntentFilter> filters = new ArrayList<>();
        filters.add(filter);

        List<ComponentName> activities = new ArrayList<>();
        final PackageManager packageManager = Manager.getPackageManager(context);

        packageManager.getPreferredActivities(filters, activities, null);

        for (ComponentName activity : activities) {
            if (packageName.equals(activity.getPackageName())) {
                return cls == null || Component.isComponentEnabled(context, cls);

            }
        }

        return false;
    }

    public static boolean isDefaultLauncher(final Context context, String packageName) {
        return isDefaultLauncher(context, packageName, null);
    }

    public static List<ResolveInfo> getAllLaunchers(final Context context) {

        final List<ResolveInfo> launchers = new ArrayList<ResolveInfo>();

        Intent filter = new Intent(Intent.ACTION_MAIN);
        filter.addCategory(Intent.CATEGORY_HOME);
        filter.addCategory(Intent.CATEGORY_DEFAULT);

        List<ResolveInfo> homeActivities = context.getPackageManager().queryIntentActivities(filter, 0x10000);
        for (ResolveInfo activity : homeActivities) {
            launchers.add(activity);
        }

        return launchers;
    }
}
