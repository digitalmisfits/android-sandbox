package com.digitalmisfits.android.common;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.WindowManager;


import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;

public class Manager {

    public static final String TAG = "Manager";

    public static PackageManager getPackageManager(final Context context) {
        return context.getPackageManager();
    }

    public static KeyguardManager getKeyguardManager(final Context context) {
        return (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
    }

    public static LocationManager getLocationManager(final Context context) {
        return (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public static LayoutInflater getLayoutInflater(final Context context) {
        return (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public static WindowManager getWindowManager(final Context context) {
        return (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    }

    public static TelephonyManager getTelephonyManager(final Context context) {
        return (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    }

    public static ITelephony getTelephonyService(final Context context) {

        TelephonyManager t = getTelephonyManager(context);

        try {

            Class cls = Class.forName(t.getClass().getName());
            Method method = cls.getDeclaredMethod("getITelephony");
            method.setAccessible(true);

            return (ITelephony) method.invoke(t);

        } catch (Exception e) {
            Log.d(TAG, "Unable to instantiate the telephony service", e);

        }

        return null;
    }
}
