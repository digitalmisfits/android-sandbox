package com.digitalmisfits.android.common;


import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.google.common.collect.Lists;
import com.digitalmisfits.pigeon.app.Settings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class System {

    private static final String TAG = "System";

    public static class ApplicationProperty implements Comparable<ApplicationProperty> {

        private Drawable icon;
        private String label;
        private String packageName;
        private String className;

        public ApplicationProperty(String packageName, String className, Drawable icon, String label) {
            setPackageName(packageName);
            setClassName(className);

            this.icon = icon;
            this.label = label;
        }

        public Drawable getIcon() {
            return icon;
        }

        public String getLabel() {
            return label;
        }


        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public int compareTo(ApplicationProperty t) {
            return label.compareToIgnoreCase(t.getLabel());
        }
    }

    /**
     * Return a list comprised of icon/ label for all the currently installed applications
     *
     * @param context
     * @return a list of application properties
     */
    public static List<ApplicationProperty> getInstalledApplications(Context context) {

        List<String> exclusion = Lists.newArrayList(context.getPackageName());
        List<ApplicationProperty> properties = new ArrayList<>();

        PackageManager pm = Manager.getPackageManager(context);
        List<ApplicationInfo> applications = pm.getInstalledApplications(0);

        List<String> allowedSystemApplications = Settings.getAllowedSystemApplications();

        for (ApplicationInfo application : applications) {

            boolean isUserPackage = ((application.flags & ApplicationInfo.FLAG_SYSTEM) == 0);

            if (isUserPackage && !exclusion.contains(application.packageName) || allowedSystemApplications.contains(application.packageName)) {

                try {
                    String packageName = application.packageName;
                    String className = application.className;
                    Drawable icon = pm.getApplicationIcon(application);
                    String label = pm.getApplicationLabel(application).toString().trim();

                    properties.add(new ApplicationProperty(packageName, className, icon, label));

                } catch (Resources.NotFoundException nfe) {
                    Log.w(TAG, nfe.getMessage(), nfe);
                }
            }
        }

        Collections.sort(properties);
        return properties;
    }

    public static void suicide() {
        // android.os.Process.killProcess(android.os.Process.myPid());
        // java.lang.System.exit(0);
    }
}
