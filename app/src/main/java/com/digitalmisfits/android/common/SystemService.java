package com.digitalmisfits.android.common;

import android.content.Context;
import android.location.LocationManager;
import android.util.Log;

public class SystemService {

    public static final String TAG = "SystemService";

    public static boolean isLocationGpsProviderEnabled(final Context context) {

        try {
            return Manager.getLocationManager(context).isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Throwable t) {
            Log.d(TAG, t.getMessage(), t);
        }

        return false;
    }
}
