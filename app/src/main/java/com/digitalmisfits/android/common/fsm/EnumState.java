package com.digitalmisfits.android.common.fsm;

import java.util.Set;

public interface EnumState<T> {

    public Set<T> possibleFollowUps();

    @Override
    public String toString();
}
