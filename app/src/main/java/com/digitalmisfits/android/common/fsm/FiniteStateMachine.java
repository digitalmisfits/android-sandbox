package com.digitalmisfits.android.common.fsm;


import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FiniteStateMachine<T> {

    public static final String TAG = "FiniteStateMachine";

    private EnumState<T> currentState;
    private EnumState<T> previousState;

    private List<StateTransitionListener> preTransitionListeners = new ArrayList<>();
    private List<StateTransitionListener> postTransitionListeners = new ArrayList<>();


    private Map<EnumState<T>, List<StateTransitionListener>> stateListeners = new HashMap<>();

    public FiniteStateMachine() {

    }

    public void addPreTransitionListener(StateTransitionListener listener) {
        preTransitionListeners.add(listener);
    }

    public void addPostTransitionListener(StateTransitionListener listener) {
        postTransitionListeners.add(listener);
    }

    public void addStateTransitionListener(EnumState<T> state, StateTransitionListener listener) {

        if(!stateListeners.containsKey(state)) {
            stateListeners.put(state, new ArrayList<StateTransitionListener>());
        }

        stateListeners.get(state).add(listener);
    }

    public boolean doTransition(EnumState<T> state, Intent intent) {

        Log.d(TAG, String.format("doTransition called for state %s", state));

        // state transition does not occur
        if(currentState != null && currentState.equals(state)) {
            return false;
        }

        if(currentState == null || currentState.possibleFollowUps().contains(state)) {

            previousState = currentState;
            currentState = state;

            for(StateTransitionListener preTransitionListener: preTransitionListeners) {
                preTransitionListener.execute(state, previousState, intent);
            }

            if(stateListeners.containsKey(state)) {
                for(StateTransitionListener listener: stateListeners.get(state)) {
                    listener.execute(state, previousState, intent);
                }
            }

            for(StateTransitionListener postTransitionListener: postTransitionListeners) {
                postTransitionListener.execute(state, previousState, intent);
            }

            return true;
        }

        return false;
    }

    public interface StateTransitionListener<T> {
        public void execute(T state, T previous, Intent intent);
    }
}
