package com.digitalmisfits.android.service;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.Gravity;
import android.view.WindowManager;

import com.digitalmisfits.android.view.StickyOverlayViewGroup;

public class StickyOverlay extends Service {

    private static final String TAG = "StickyOverlay";

    private StickyOverlayViewGroup stickyOverlay;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        stickyOverlay = new StickyOverlayViewGroup(this);

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.RIGHT | Gravity.TOP;
        params.width = 1;
        params.height = 1;

        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.addView(stickyOverlay, params);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (stickyOverlay != null) {
            ((WindowManager) getSystemService(WINDOW_SERVICE)).removeView(stickyOverlay);
            stickyOverlay = null;
        }
    }
}

