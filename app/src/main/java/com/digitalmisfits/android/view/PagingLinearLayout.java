package com.digitalmisfits.android.view;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

import com.digitalmisfits.pigeon.widget.ToggableImageView;

public class PagingLinearLayout extends LinearLayout implements ViewPager.OnPageChangeListener {

    private static final String TAG = "PagingLinearLayout";

    private ViewPager viewPager;

    public PagingLinearLayout(android.content.Context context) {
        super(context);
        init();
    }

    public PagingLinearLayout(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void init() {
        // pass
    }

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    public void onPageSelected(int position) {

        int childcount = getChildCount();

        for (int i = 0; i < childcount; i++) {
            View v = getChildAt(i);
            if (v instanceof ToggableImageView) {
                ((ToggableImageView) v).setActive(false);
            }
        }

        View v = getChildAt(position);
        if (v != null && v instanceof ToggableImageView) {
            ((ToggableImageView) v).setActive(true);
        }
    }

    public void onPageScrollStateChanged(int state) {
    }

    public ViewPager getViewPager() {
        return viewPager;
    }

    public void setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;

        if (this.viewPager != null) {

            int childcount = getChildCount();

            for (int i = 0; i < childcount; i++) {
                View v = getChildAt(i);
                final int item = i;
                if (v instanceof ToggableImageView) {
                    v.setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            PagingLinearLayout.this.viewPager.setCurrentItem(item);
                        }
                    });
                }
            }
        }

        assert this.viewPager != null;
        this.viewPager.setOnPageChangeListener(this);
    }
}
