package com.digitalmisfits.android.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.ViewGroup;

public class StickyOverlayViewGroup extends ViewGroup {

    private static final String TAG = "StickyOverlayViewGroup";


    public StickyOverlayViewGroup(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Bitmap buffer = Bitmap.createBitmap(new int[] {(0 & 0xFF) << 24}, 1, 1, Bitmap.Config.ALPHA_8);
        canvas.drawBitmap(buffer, 0, 0, null);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {

    }
}



