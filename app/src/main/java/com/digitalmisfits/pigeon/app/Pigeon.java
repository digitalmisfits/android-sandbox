package com.digitalmisfits.pigeon.app;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.digitalmisfits.pigeon.service.messaging.GcmRegistrationService;

import org.acra.*;
import org.acra.annotation.*;

@ReportsCrashes(formKey = "", formUri = "http://acra.digitalmisfits.com/crash/add")
public class Pigeon extends Application {

    public static final String TAG = "Pigeon";

    private static Context context;

    public static Context getCustomApplicationContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        ACRA.init(this);

        context = getApplicationContext();

        /*
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable ex) {
                Log.d(TAG, "Handling uncaught exception", ex);
                System.exit(2);
            }
        });
        */

        /*
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        filter.setPriority(999);
        registerReceiver(new SystemDialogCloseReceiver(), filter);
        */

        startServices();

        Log.d(TAG, "Application started");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }

    protected void startServices() {

        /* Google Cloud Messaging registration service */
        startService(new Intent(getApplicationContext(), GcmRegistrationService.class));

        /* WindowCapture (Touch Event Hook)
        startService(new Intent(getApplicationContext(), WindowCapture.class));
        */

        /*
        startService(new Intent(getApplicationContext(), Guard.class));
        startService(new Intent(getApplicationContext(), IdleTimeout.class));
        */
    }
}

