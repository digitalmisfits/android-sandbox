package com.digitalmisfits.pigeon.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.util.Log;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.digitalmisfits.pigeon.store.ProfileStore;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class Settings {

    public static final String TAG = "Settings";

    public static final String GCM_SENDER_ID = "656816925796";

    private static final Gson gson = new Gson();

    public static Class<ProfileStore> getProfile() {
        return ProfileStore.class;
    }

    public static void clear() {
        getSharedPreferences().edit().clear().commit();
    }


    /**
     * Return the GoogleCloudMessaging Registration Identifier
     *
     * @param context
     * @return the registration id
     */
    public static String getGcmRegistrationId(Context context) {
        final SharedPreferences prefs = getSharedPreferences();

        String registrationId = prefs.getString("GcmRegistrationId", "");

        if (registrationId.isEmpty()) {
            return "";
        }

        int registeredVersion = prefs.getInt("appVersion", Integer.MIN_VALUE);
        int currentVersion = getApplicationVersion(context);

        if (registeredVersion != currentVersion) {
            return "";
        }

        return registrationId;
    }

    public static void setGcmRegistrationId(Context context, String registrationId) {
        Editor editor = getSharedPreferences().edit();
        editor.putString("GcmRegistrationId", registrationId);
        editor.putInt("appVersion", getApplicationVersion(context));
        editor.commit();
    }

    /**
     *
     * @return a list of packages which are not blocked by the Guard
     */
    public static List<String> getWhiteListedSystemPackages() {

        List<String>  packages=  Lists.newArrayList(
            "android",
            Pigeon.getCustomApplicationContext().getPackageName()
        );

        packages.addAll(getDailerSystemPackages());

        return packages;
    }

    public static List<String> getDailerSystemPackages() {
        return Lists.newArrayList(
                "com.android.phone",
                "com.google.android.dialer",
                "com.android.dialer"
        );
    }

    /**
     *
     * @return a list of system packages which can be used in the application selector
     */
    public static List<String> getAllowedSystemApplications() {
        return Lists.newArrayList(
            "com.google.android.apps.maps"
        );
    }

    public static Map<String, Map<String, String>> getIcons() {

       return gson.fromJson(getJsonFromAsset("icons.json"), new TypeToken<Map<String, Map<String, String>>>(){}.getType());
    }

    public static String getJsonFromAsset(String file) {

        AssetManager am = Pigeon.getCustomApplicationContext().getAssets();

        InputStream input = null;
        try {
            input = am.open(file);
            if(input.available() > 0) {
                byte[] buf = new byte[input.available()];
                input.read(buf);

                return new String(buf, "UTF-8");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                Log.d(TAG, e.getMessage(), e);
            }
        }

        return null;
    }

    public static SharedPreferences getSharedPreferences() {
        return Pigeon.getCustomApplicationContext().getSharedPreferences(Settings.class.getName(), Context.MODE_PRIVATE);
    }

    public static int getApplicationVersion(Context context) {

        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static String getApplicationVersionName(Context context) {

        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
}
