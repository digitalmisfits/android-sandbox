package com.digitalmisfits.pigeon.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.digitalmisfits.android.common.Manager;
import com.digitalmisfits.pigeon.app.R;


import java.lang.reflect.Method;

public class DailerActivity extends Activity {

    public static final String TAG = "DailerActivity";

    private TelephonyManager telephonyManager;
    private ITelephony telephonyService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dailer);

        // bind to the internal telephony subsystem
        telephonyManager = Manager.getTelephonyManager(getApplicationContext());
        bindToTelephonyService();
    }

    protected void bindToTelephonyService() {

        try {
            Class c = Class.forName(telephonyManager.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            telephonyService = (ITelephony) m.invoke(telephonyManager);

        } catch (Exception e) {
            Log.d(TAG, "Unable to connect to telephony subsystem", e);
            finish();
        }
    }

    protected void aidlAnswer() {

        try {
            /*
            try {
                telephonyService.silenceRinger();
            } catch (SecurityException e) {
                Log.d(TAG, "Unable to silence ringer", e);
            }
            */
            telephonyService.answerRingingCall();

        } catch (RemoteException e) {
            Log.e("call prompt","Exception object: "+e);
        }
    }
}

