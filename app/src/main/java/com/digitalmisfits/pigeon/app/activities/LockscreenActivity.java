package com.digitalmisfits.pigeon.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowManager;

import com.digitalmisfits.pigeon.app.R;

public class LockscreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lockscreen);
    }

    @Override
    public void onAttachedToWindow() {

        int flags = WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED;
        getWindow().addFlags(flags);

        super.onAttachedToWindow();
    }
}
