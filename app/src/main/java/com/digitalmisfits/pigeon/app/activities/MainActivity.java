package com.digitalmisfits.pigeon.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.digitalmisfits.android.common.Launcher;
import com.digitalmisfits.pigeon.app.R;
import com.digitalmisfits.pigeon.app.Settings;
import com.digitalmisfits.pigeon.app.activities.child.HomeActivity;
import com.digitalmisfits.pigeon.app.activities.guardian.GuardianActivity;
import com.digitalmisfits.pigeon.app.activities.setup.ActorSelectionActivity;
import com.digitalmisfits.pigeon.app.activities.setup.LauncherSetupActivity;
import com.digitalmisfits.pigeon.store.ProfileStore;
import com.digitalmisfits.pigeon.typedef.Actor;
import com.digitalmisfits.pigeon.widget.PigeonTextView;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private final static int SPLASH_TIME_OUT = 2500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        PigeonTextView tv = (PigeonTextView) findViewById(R.id.version);
        tv.setText(String.format("Version: %s", Settings.getApplicationVersionName(this)));


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Actor actor = ProfileStore.getActor();

                if (actor == null) {

                    Intent intent = new Intent(MainActivity.this, ActorSelectionActivity.class);
                    startActivity(intent);

                } else {
                    switch (actor) {
                        case GUARDIAN: {
                            Intent intent = new Intent(MainActivity.this, GuardianActivity.class);
                            startActivity(intent);

                            break;
                        }

                        case CHILD: {

                            if(Launcher.isDefaultLauncher(MainActivity.this, getPackageName(), HomeActivity.class)) {
                                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(MainActivity.this, LauncherSetupActivity.class);
                                startActivity(intent);
                            }
                            break;
                        }
                    }
                }

                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //checkPlayServices();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {

            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }

            return false;
        }

        return true;
    }
}

