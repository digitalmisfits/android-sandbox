package com.digitalmisfits.pigeon.app.activities.child;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.digitalmisfits.android.common.Launcher;
import com.digitalmisfits.android.view.PagingLinearLayout;
import com.digitalmisfits.pigeon.app.R;
import com.digitalmisfits.pigeon.app.activities.child.dialog.AuthenticateDialog;
import com.digitalmisfits.pigeon.app.activities.child.preferences.PreferencesActivity;
import com.digitalmisfits.pigeon.service.Guard;


public class HomeActivity extends FragmentActivity implements AuthenticateDialog.AuthenticateDialogListener {

    private static final String TAG = "HomeActivity";

    private static final int FRAGMENTS = 3;

    private HomeFragmentPagerAdapter adapter;
    private ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String intentOrigin = getIntent().getStringExtra("origin");
        if (intentOrigin != null && intentOrigin.equals("com.android.internal.app.ResolverActivity")) {
            if (!Launcher.isDefaultLauncher(this, getPackageName())) {
                Log.d(TAG, "ResolverActivity: Home is not the default launcher");
                finish();
                return;
            }

        }

        /* Initialise the Guard */
        Intent initIntent = new Intent(this, Guard.class);
        initIntent.setAction(Guard.GuardLifecycleState.INIT.getAction());
        startService(initIntent);

        // built layout

        pager = (ViewPager) findViewById(R.id.viewPager);

        PagingLinearLayout navigation = (PagingLinearLayout) findViewById(R.id.navigation);
        navigation.setViewPager(pager);

        // Add adapter to the pager and set the selected item to 1
        adapter = new HomeFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setCurrentItem(1);


        ImageView settingsBtn = (ImageView) findViewById(R.id.settingsBtn);
        settingsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AuthenticateDialog dialog = new AuthenticateDialog();
                dialog.show(getSupportFragmentManager(), "Authenticate");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        /* Start the Guard */
        Intent startIntent = new Intent(this, Guard.class);
        startIntent.setAction(Guard.GuardLifecycleState.START.getAction());
        startService(startIntent);

        if(adapter != null)
            adapter.notifyDataSetChanged();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        final boolean handled = super.onKeyDown(keyCode, event);

        // Eat the long press event so the keyboard doesn't come up.
        if (keyCode == KeyEvent.KEYCODE_MENU && event.isLongPress()) {
            return true;
        }

        return handled;
    }

    @Override
    public void onBackPressed() {
        // disable
    }

    public class HomeFragmentPagerAdapter extends FragmentPagerAdapter {

        public HomeFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return FRAGMENTS;
        }

        @Override
        public Fragment getItem(int position) {

            Log.d(TAG, String.format("Fragment switch requested %d", position));

            switch (position) {
                case 0: {
                    return new HomeLocationFragment();
                }
                case 2: {
                    return new HomeApplicationFragment();
                }
                default: {
                    return new HomeContactFragment();
                }
            }
        }
    }

    @Override
    public void onAuthenticateDialogOkClick(String password) {
        Intent intent = new Intent(this, PreferencesActivity.class);
        startActivity(intent);
    }
}
