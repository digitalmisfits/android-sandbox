package com.digitalmisfits.pigeon.app.activities.child;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalmisfits.android.common.Manager;
import com.digitalmisfits.android.common.System;
import com.digitalmisfits.pigeon.app.R;
import com.digitalmisfits.pigeon.app.activities.child.fragment.AbstractContainerFragment;
import com.digitalmisfits.pigeon.store.ProfileStore;

import java.util.List;

public class HomeApplicationFragment extends AbstractContainerFragment {

    public static final String TAG = "HomeApplicationFragment";

    private ArrayAdapter<String> adapter;
    private List<System.ApplicationProperty> applications;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (adapter != null) {
            adapter.clear();
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
                for(String packageName: ProfileStore.getApplications()) {
                    adapter.add(packageName);
                }
            } else {
                adapter.addAll(ProfileStore.getApplications());
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_viewpager_grid, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        applications = System.getInstalledApplications(getActivity());
        adapter = new ApplicationAdapter(getActivity(), ProfileStore.getApplications());

        GridView gridview = (GridView) getView().findViewById(R.id.gridview);
        gridview.setOnItemLongClickListener(new android.widget.AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {

                String packageName = ProfileStore.getApplications().get(position);

                Intent intent = Manager.getPackageManager(getActivity()).getLaunchIntentForPackage(packageName);

                if (null != intent) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_USER_ACTION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    getActivity().startActivity(intent);
                }

                return true;
            }
        });

        gridview.setAdapter(adapter);
    }

    @Override
    public int getCalculatedComponentHeight() {
        return getNavigationBarHeight();
    }

    public class ApplicationAdapter extends AbstractGridViewAdapter<String> {

        private String[] colors = {
                "#ff9300",
                "#6ca437",
                "#5ec6ff",
                "#de5d59",
        };

        private class ViewHolder {
            RelativeLayout canvas;
            ImageView icon;
            TextView label;
        }

        public ApplicationAdapter(Context context, List<String> values) {
            super(context, R.layout.layout_square_grid_item, values);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            String packageName = getItem(position);

            ViewHolder holder;
            if (convertView == null) {

                holder = new ViewHolder();

                convertView = Manager.getLayoutInflater(getActivity()).inflate(R.layout.layout_square_grid_item, parent, false);
                holder.icon = (ImageView) convertView.findViewById(R.id.icon);
                holder.label = (TextView) convertView.findViewById(R.id.label);
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            for (System.ApplicationProperty property : applications) {
                if (property.getPackageName().equals(packageName)) {
                    holder.label.setText(property.getLabel());

                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        holder.icon.setBackgroundDrawable(property.getIcon());
                    } else {
                        holder.icon.setBackground(property.getIcon());
                    }

                    break;
                }
            }

            convertView.setLayoutParams(new AbsListView.LayoutParams(getContainerWidth(), getContainerHeight()));
            convertView.setBackgroundColor(Color.parseColor(colors[position % colors.length]));

            return convertView;
        }
    }
}