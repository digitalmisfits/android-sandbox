package com.digitalmisfits.pigeon.app.activities.child;


import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalmisfits.android.common.Manager;
import com.digitalmisfits.pigeon.app.R;
import com.digitalmisfits.pigeon.app.activities.child.fragment.AbstractContainerFragment;
import com.digitalmisfits.pigeon.store.ProfileStore;

import java.util.LinkedList;
import java.util.List;

public class HomeContactFragment extends AbstractContainerFragment {

    private static final String TAG = "HomeContactFragment";

    private ContactAdapter adapter;
    private LinkedList<ProfileStore.Contact> values = new LinkedList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (adapter != null) {
            adapter.clear();

            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
                for(ProfileStore.Contact contact: ProfileStore.getContacts()) {
                    adapter.add(contact);
                }
            } else {
                adapter.addAll(ProfileStore.getContacts());
            }

            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_viewpager_grid, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        GridView gridview = (GridView) getView().findViewById(R.id.gridview);
        gridview.setOnItemLongClickListener(new android.widget.AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {

                ProfileStore.Contact contact = ProfileStore.getContacts().get(position);

                if (!contact.isPlaceholder) {
                    FragmentManager fragmentManager = getFragmentManager();
                    ActionOverlayDialogFragment f = new ActionOverlayDialogFragment();

                    final Bundle args = new Bundle();
                    args.putSerializable("contact", contact);
                    f.setArguments(args);

                    f.show(fragmentManager, "dialog");
                }

                return true;
            }
        });

        adapter = new ContactAdapter(getActivity(), ProfileStore.getContacts());
        gridview.setAdapter(adapter);
    }

    @Override
    public int getCalculatedComponentHeight() {
        return getNavigationBarHeight();
    }

    public class ContactAdapter extends AbstractGridViewAdapter<ProfileStore.Contact> {

        private class ViewHolder {
            RelativeLayout canvas;
            ImageView icon;
            TextView label;
        }

        public ContactAdapter(Context context, List<ProfileStore.Contact> values) {
            super(context, values);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ProfileStore.Contact contact = getItem(position);

            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = Manager.getLayoutInflater(getActivity()).inflate(R.layout.layout_square_grid_item, parent, false);
                holder.icon = (ImageView) convertView.findViewById(R.id.icon);
                holder.label = (TextView) convertView.findViewById(R.id.label);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            String resource = contact.resource;
            String label = contact.label;
            String color = contact.color;

            if (contact.isPlaceholder) {
                resource = "empty";
                label = "empty".toUpperCase();
                color = "#FFFFFF";
            }

            int drawableId = getResources().getIdentifier(resource, "drawable", getContext().getPackageName());

            holder.label.setText(label);
            holder.icon.setBackgroundResource(drawableId);

            convertView.setLayoutParams(new AbsListView.LayoutParams(getContainerWidth(), getContainerHeight()));
            convertView.setBackgroundColor(Color.parseColor(color));

            return convertView;
        }
    }

    static public class ActionOverlayDialogFragment extends DialogFragment {

        private ProfileStore.Contact contact;

        public ActionOverlayDialogFragment() {

        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            contact = (ProfileStore.Contact) getArguments().get("contact");
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_overlay_contact, container, false);
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog dialog = super.onCreateDialog(savedInstanceState);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            return dialog;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            final ImageView doAction = (ImageView) getView().findViewById(R.id.action_do);
            final ImageView cancelAction = (ImageView) getView().findViewById(R.id.action_cancel);

            doAction.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {

                    String uri = String.format("tel:%s", contact.phone);
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setComponent(new ComponentName("com.android.phone", "com.android.phone.OutgoingCallBroadcaster"));
                    intent.setData(Uri.parse(uri));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    startActivity(intent);

                    if (getDialog().isShowing()) {
                        getDialog().dismiss();
                    }
                }
            });

            cancelAction.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (getDialog().isShowing()) {
                        getDialog().dismiss();
                    }
                }
            });
        }
    }
}