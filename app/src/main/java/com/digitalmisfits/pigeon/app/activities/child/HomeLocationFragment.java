package com.digitalmisfits.pigeon.app.activities.child;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.IntentSender;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.digitalmisfits.android.common.Manager;
import com.digitalmisfits.pigeon.app.R;
import com.digitalmisfits.pigeon.app.activities.child.fragment.AbstractContainerFragment;
import com.digitalmisfits.pigeon.client.DefaultPigeonClient;
import com.digitalmisfits.pigeon.model.Location;
import com.digitalmisfits.pigeon.store.ProfileStore;

import java.util.List;


public class HomeLocationFragment extends AbstractContainerFragment implements GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener {

    private static final String TAG = "HomeLocationFragment";

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    private LocationClient locationClient;

    @Override
    public void onConnected(Bundle dataBundle) {
        Log.d(TAG, "GooglePlayServicesClient connected");
    }

    @Override
    public void onDisconnected() {
        Log.d(TAG, "GooglePlayServicesClient disconnected");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                Log.d(TAG, e.getMessage(), e);
            }
        } else {
            Log.d(TAG, connectionResult.toString());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
         * Create a new location client, using the enclosing class to
         * handle callbacks.
         */
        locationClient = new LocationClient(getActivity(), this, this);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Connect the client.
        locationClient.connect();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        GridView gridview = (GridView) getView().findViewById(R.id.gridview);
        gridview.setOnItemLongClickListener(new android.widget.AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                FragmentManager fragmentManager = getFragmentManager();

                ActionOverlayDialogFragment f = new ActionOverlayDialogFragment();
                f.show(fragmentManager, "dialog");
                return true;
            }
        });

        gridview.setAdapter(new LocationAdapter(getActivity(), ProfileStore.getLocations()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_viewpager_grid, container, false);
    }

    @Override
    public void onStop() {
        // Disconnecting the client invalidates it.
        locationClient.disconnect();

        super.onStop();
    }

    public class LocationAdapter extends AbstractGridViewAdapter<ProfileStore.Location> {

        private List<ProfileStore.Location> locations;

        private class ViewHolder {
            RelativeLayout canvas;
            ImageView icon;
            TextView label;
        }

        public LocationAdapter(Context context, List<ProfileStore.Location> locations) {
            super(context, R.layout.layout_square_grid_item, locations);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ProfileStore.Location location = getItem(position);

            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = Manager.getLayoutInflater(getActivity()).inflate(R.layout.layout_square_grid_item, parent, false);
                holder.icon = (ImageView) convertView.findViewById(R.id.icon);
                holder.label = (TextView) convertView.findViewById(R.id.label);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            String resource = location.resource;
            String label = location.label;
            String color = location.color;

            if (location.isPlaceholder) {
                resource = "empty_location";
                label = "empty".toUpperCase();
                color = "#FFFFFF";
            }

            int drawableId = getResources().getIdentifier(resource, "drawable", getContext().getPackageName());

            holder.label.setText(label);
            holder.icon.setBackgroundResource(drawableId);

            convertView.setLayoutParams(new AbsListView.LayoutParams(getContainerWidth(), getContainerHeight()));
            convertView.setBackgroundColor(Color.parseColor(color));

            return convertView;
        }
    }

    @SuppressLint("ValidFragment")
    public class ActionOverlayDialogFragment extends DialogFragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_overlay_location, container, false);
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog dialog = super.onCreateDialog(savedInstanceState);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            return dialog;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            final ImageView doAction = (ImageView) getView().findViewById(R.id.action_do);
            final ImageView cancelAction = (ImageView) getView().findViewById(R.id.action_cancel);

            doAction.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                try {
                    new CheckInAsyncTask().execute();
                } catch (Exception e) {
                    Log.d(TAG, e.getMessage(), e);
                }

                if (getDialog().isShowing()) {
                    getDialog().dismiss();
                }
                }
            });

            cancelAction.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                if (getDialog().isShowing()) {
                    getDialog().dismiss();
                }
                }
            });
        }
    }

    private class CheckInAsyncTask extends AsyncTask<Void, Boolean, Boolean> {

        protected void onPreExecute() {

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            android.location.Location lastLocation = null;

            try {
                lastLocation = locationClient.getLastLocation();
            } catch (IllegalStateException e) {
                Log.d(TAG, e.getMessage(), e);
            }

            if(lastLocation == null) {
                Log.d(TAG, "No location available");
                return false;
            }

            Location location = new Location();
            location.setLat(lastLocation.getLatitude());
            location.setLng(lastLocation.getLongitude());

            String uuid = ProfileStore.getUUID();
            String name = ProfileStore.getName();

            Log.d(TAG, String.format("uuid %s, name %s", uuid, name));

            DefaultPigeonClient.getInstance().checkin(uuid, name, location);

            return true;
        }

        protected void onPostExecute(Boolean result) {
            Log.d(TAG, "onPostExecute checkin");

            if(result) {
                Toast.makeText(getActivity(), "Successfully checked-in", 5000).show();
            } else {
                Toast.makeText(getActivity(), "Location not available", 5000).show();
            }
        }
    }
}
