package com.digitalmisfits.pigeon.app.activities.child.fragment;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Display;
import android.widget.ArrayAdapter;

import com.digitalmisfits.android.common.Manager;
import com.digitalmisfits.pigeon.app.R;

import java.util.List;


public class AbstractContainerFragment extends Fragment {

    /**
     * @return the height of the bottom navigation bar (usually 60dp) in px
     */
    public int getNavigationBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.navigation_height);
    }

    /**
     * @return the height of the built-in status bar in px
     */
    public int getStatusBarHeight() {

        int statusBarHeight = 0;

        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = getResources().getDimensionPixelSize(resourceId);

        }
        return statusBarHeight;
    }

    /**
     * @return the height of the action bar in px
     */
    public int getActionBarHeight() {

        int actionBarHeight = 0;

        TypedValue typedValue = new TypedValue();
        if (getActivity().getTheme().resolveAttribute(R.attr.actionBarSize, typedValue, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(typedValue.data, getResources().getDisplayMetrics());
        }

        return actionBarHeight;
    }

    /**
     * @return the consolidated height of a generic preference window
     */
    public int getCalculatedComponentHeight() {
        return getStatusBarHeight() + getNavigationBarHeight() + getActionBarHeight();
    }

    /**
     * ArrayAdapter implementation with dynamic fragment calculation
     *
     * @param <T>
     */
    public class AbstractGridViewAdapter<T> extends ArrayAdapter<T> {

        protected static final String TAG = "AbstractGridViewAdapter";
        protected static final int MIN_ROWS = 2;

        protected int containerHeight = 0;
        protected int containerWidth = 0;

        public AbstractGridViewAdapter(Context context, List<T> values) {
            this(context, R.layout.layout_square_grid_item, values);
        }

        public AbstractGridViewAdapter(Context context, int resourceId, List<T> values) {
            super(context, resourceId, values);
            init();
        }

        public void init() {
            Display display = Manager.getWindowManager(getContext()).getDefaultDisplay();

            int height = 0;
            int width = 0;

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                Point size = new Point();
                display.getSize(size);
                width = size.x;
                height = size.y;
            } else {
                width = display.getWidth();
                height = display.getHeight();
            }

            setContainerWidth(width / MIN_ROWS);
            setContainerHeight((height - getCalculatedComponentHeight()) / MIN_ROWS);
        }


        public int getContainerHeight() {
            return containerHeight;
        }

        public void setContainerHeight(int containerHeight) {
            this.containerHeight = containerHeight;
        }

        public int getContainerWidth() {
            return containerWidth;
        }

        public void setContainerWidth(int containerWidth) {
            this.containerWidth = containerWidth;
        }

    }
}
