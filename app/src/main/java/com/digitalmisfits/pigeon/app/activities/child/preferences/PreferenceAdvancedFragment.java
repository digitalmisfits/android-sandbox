package com.digitalmisfits.pigeon.app.activities.child.preferences;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.Preference;
import android.util.Log;

import com.github.machinarius.preferencefragment.PreferenceFragment;
import com.digitalmisfits.pigeon.app.R;
import com.digitalmisfits.pigeon.app.activities.setup.LauncherSetupActivity;
import com.digitalmisfits.pigeon.client.DefaultPigeonClient;
import com.digitalmisfits.pigeon.service.Guard;
import com.digitalmisfits.pigeon.service.scheduler.GcmRegistrationAlarmScheduler;
import com.digitalmisfits.pigeon.store.ProfileStore;

public class PreferenceAdvancedFragment extends PreferenceFragment {

    private static final String TAG = "PreferenceAdvancedFragment";

    BroadcastReceiver uninstallBroadcastReceiver;
    private boolean mIsRunning = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Context context = getActivity().getApplicationContext();

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences_child);

        Preference code = (Preference) findPreference("code");
        code.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(android.preference.Preference preference) {

                new TokenGenerateAsyncTask().execute();

                return true;
            }
        });

        Preference uninstall = (Preference) findPreference("uninstall");
        uninstall.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                Intent stopIntent = new Intent(getActivity(), Guard.class);
                stopIntent.setAction(Guard.GuardLifecycleState.STOP.getAction());
                getActivity().startService(stopIntent);

                Uri packageURI = Uri.parse(String.format("package:%s", context.getPackageName()));
                Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
                getActivity().startActivityForResult(uninstallIntent, 0x00);

                return true;
            }
        });

        Preference exit = (Preference) findPreference("exit");
        exit.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(android.preference.Preference preference) {

                // stop guard services
                Intent intent = new Intent(context, Guard.class);
                intent.setAction(Guard.GuardLifecycleState.KILL.getAction());
                intent.putExtra("disableLauncherComponent", true);
                getActivity().startService(intent);

                // stop any pending GCM Registration alarms
                GcmRegistrationAlarmScheduler.unschedule(context);

                new ResetToDefaultAsyncTask().execute();
                return true;
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();

        mIsRunning = true;
    }


    @Override
    public void onPause() {
        super.onPause();

        mIsRunning = false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 0 && resultCode == 0) {
            Intent startIntent = new Intent(getActivity(), Guard.class);
            startIntent.setAction(Guard.GuardLifecycleState.START.getAction());
            getActivity().startService(startIntent);
        }
    }

    private class ResetToDefaultAsyncTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog dialog;

        protected void onPreExecute() {
            dialog = new ProgressDialog(PreferenceAdvancedFragment.this.getActivity());
            dialog.setTitle("System");
            dialog.setMessage("Unlocking Pigeon...");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.show();
        }

        protected Void doInBackground(Void... params) {

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                Log.d(TAG, "Waiting time interrupted");
            }

            return null;
        }

        protected void onPostExecute(Void result) {

            if (dialog.isShowing())
                dialog.dismiss();

            Intent intent = new Intent(getActivity(), LauncherSetupActivity.class);
            intent.putExtra("exit", "true");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

            getActivity().finish();
        }
    }

    private class TokenGenerateAsyncTask extends AsyncTask<Void, String, String> {

        AlertDialog dialog;


        protected void onPreExecute() {
            dialog = new ProgressDialog.Builder(PreferenceAdvancedFragment.this.getActivity())
                    .setTitle("System")
                    .setMessage("Generating code..")
                    .show();
        }

        protected String doInBackground(Void... paramses) {

            String uuid = ProfileStore.getUUID();

            if (!"".equals(uuid)) {

                String token = DefaultPigeonClient.getInstance().getToken(uuid);
                if (!"".equals(token)) {
                    return token;
                }
            }
            return null;
        }

        protected void onPostExecute(String result) {

            if (dialog.isShowing())
                dialog.dismiss();

            if (!"".equals(result)) {

                AlertDialog.Builder builder = new AlertDialog.Builder(PreferenceAdvancedFragment.this.getActivity());
                builder
                        .setTitle("Code")
                        .setMessage(result)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
}