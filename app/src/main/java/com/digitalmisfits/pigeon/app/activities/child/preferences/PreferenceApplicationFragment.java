package com.digitalmisfits.pigeon.app.activities.child.preferences;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.digitalmisfits.android.common.Manager;
import com.digitalmisfits.android.common.System;
import com.digitalmisfits.pigeon.app.R;
import com.digitalmisfits.pigeon.app.activities.child.fragment.AbstractContainerFragment;
import com.digitalmisfits.pigeon.store.ProfileStore;

import java.util.List;


public class PreferenceApplicationFragment extends AbstractContainerFragment {

    private static final String TAG = "PreferenceApplicationFragment";

    private ArrayAdapter<System.ApplicationProperty> adapter;
    private List<String> values;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_viewpager_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        adapter = new ApplicationAdapter(getActivity(), System.getInstalledApplications(getActivity()));

        listView = (ListView) getView().findViewById(R.id.listview);
        listView.setAdapter(adapter);
    }

    public class ApplicationAdapter extends AbstractGridViewAdapter<System.ApplicationProperty> {

        private class ViewHolder {
            public ImageView icon;
            public TextView label;
            public CheckBox checkbox;
        }

        public ApplicationAdapter(Context context, List<System.ApplicationProperty> applications) {
            super(context, R.layout.layout_application_row, applications);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;
            if (convertView == null) {
                convertView = Manager.getLayoutInflater(getContext()).inflate(R.layout.layout_application_row, parent, false);

                holder = new ViewHolder();

                holder.icon = (ImageView) convertView.findViewById(R.id.application_icon);
                holder.label = (TextView) convertView.findViewById(R.id.application_label);
                holder.checkbox = (CheckBox) convertView.findViewById(R.id.application_enabled);

                holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton checkboxView, boolean isChecked) {
                        System.ApplicationProperty property = (System.ApplicationProperty) checkboxView.getTag();

                        List<String> applications = ProfileStore.getApplications();

                        String packageName = property.getPackageName();
                        if (isChecked) {
                            if (!applications.contains(packageName)) {
                                applications.add(packageName);
                            }
                        } else {
                            applications.remove(packageName);
                        }

                        ProfileStore.setApplications(applications);
                    }
                });

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            System.ApplicationProperty property = getItem(position);

            holder.icon.setImageDrawable(property.getIcon());
            holder.label.setText(property.getLabel());
            holder.checkbox.setTag(property);
            holder.checkbox.setChecked(ProfileStore.getApplications().contains(property.getPackageName()));

            return convertView;
        }
    }
}