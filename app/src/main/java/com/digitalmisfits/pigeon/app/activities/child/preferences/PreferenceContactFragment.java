package com.digitalmisfits.pigeon.app.activities.child.preferences;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalmisfits.android.common.Manager;
import com.digitalmisfits.pigeon.app.R;
import com.digitalmisfits.pigeon.app.Settings;
import com.digitalmisfits.pigeon.app.activities.child.fragment.AbstractContainerFragment;
import com.digitalmisfits.pigeon.store.ProfileStore;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class PreferenceContactFragment extends AbstractContainerFragment {

    private static final String TAG = "PreferenceContactFragment";

    private ContactAdapter contactAdapter;
    private LinkedList<ProfileStore.Contact> values = new LinkedList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_viewpager_grid, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        values = ProfileStore.getContacts();

        GridView gridview = (GridView) getView().findViewById(R.id.gridview);
        gridview.setOnItemLongClickListener(new android.widget.AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                FragmentManager fragmentManager = getFragmentManager();

                ActionOverlayDialogFragment f = new ActionOverlayDialogFragment(view, position);
                f.show(fragmentManager, "dialog");
                return true;
            }
        });

        contactAdapter = new ContactAdapter(getActivity(), values);
        gridview.setAdapter(contactAdapter);
    }

    public class ContactAdapter extends AbstractGridViewAdapter<ProfileStore.Contact> {

        private class ViewHolder {
            RelativeLayout canvas;
            ImageView icon;
            TextView label;
        }

        public ContactAdapter(Context context, List<ProfileStore.Contact> values) {
            super(context, values);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ProfileStore.Contact contact = getItem(position);

            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = Manager.getLayoutInflater(getActivity()).inflate(R.layout.layout_square_grid_item, parent, false);
                holder.icon = (ImageView) convertView.findViewById(R.id.icon);
                holder.label = (TextView) convertView.findViewById(R.id.label);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            String resource = contact.resource;
            String label = contact.label;

            if (contact.isPlaceholder) {
                resource = "action_add_contact";
                label = "Add contact".toUpperCase();
            }

            int drawableId = getResources().getIdentifier(resource, "drawable", getContext().getPackageName());

            holder.label.setText(label);
            holder.icon.setBackgroundResource(drawableId);

            convertView.setLayoutParams(new AbsListView.LayoutParams(containerWidth, containerHeight));
            convertView.setBackgroundColor(getResources().getColor(R.color.background));

            return convertView;
        }
    }

    @SuppressLint("ValidFragment")
    public class ActionOverlayDialogFragment extends DialogFragment {

        private int position;

        public ActionOverlayDialogFragment(View view, int position) {
            this.position = position;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_overal_add_contact, container, false);
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog dialog = super.onCreateDialog(savedInstanceState);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            return dialog;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            final EditText name = (EditText) getView().findViewById(R.id.name);
            final EditText phone = (EditText) getView().findViewById(R.id.phone);
            final RadioGroup icons = (RadioGroup) getView().findViewById(R.id.icons);

            LinearLayout saveBtn = (LinearLayout) getView().findViewById(R.id.save);
            saveBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {

                    ProfileStore.Contact contact = values.get(position);

                    RadioButton icon = (RadioButton) icons.findViewById(icons.getCheckedRadioButtonId());
                    if(icon != null) {
                        Map<String, String> properties =  Settings.getIcons().get((String) icon.getTag());

                        contact.resource = properties.get("icon");
                        contact.color = properties.get("color");
                    } else {
                        contact.resource = "boy";
                        contact.color = "#fdc100";
                    }

                    contact.label = name.getText().toString();
                    contact.phone = phone.getText().toString();
                    contact.isPlaceholder = false;

                    ProfileStore.setContacts(values);
                    contactAdapter.notifyDataSetChanged();

                    if (getDialog().isShowing())
                        getDialog().dismiss();
                }
            });
        }
    }
}
