package com.digitalmisfits.pigeon.app.activities.child.preferences;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalmisfits.android.common.Manager;
import com.digitalmisfits.pigeon.app.R;
import com.digitalmisfits.pigeon.app.activities.child.fragment.AbstractContainerFragment;
import com.digitalmisfits.pigeon.store.ProfileStore;

import java.util.LinkedList;
import java.util.List;

public class PreferenceLocationFragment extends AbstractContainerFragment {

    private static final String TAG = "PreferenceLocationFragment";

    private LocationAdapter contactAdapter;
    private List<ProfileStore.Location> values = new LinkedList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_viewpager_grid, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        values = ProfileStore.getLocations();

        GridView gridview = (GridView) getView().findViewById(R.id.gridview);
        /*
        gridview.setOnItemLongClickListener(new android.widget.AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                FragmentManager fragmentManager = getFragmentManager();

                ActionOverlayDialogFragment f = new ActionOverlayDialogFragment(view, position);
                f.show(fragmentManager, "dialog");
                return true;
            }
        });
        */

        contactAdapter = new LocationAdapter(getActivity(), values);
        gridview.setAdapter(contactAdapter);
    }

    public class LocationAdapter extends AbstractGridViewAdapter<ProfileStore.Location> {

        private class ViewHolder {
            RelativeLayout canvas;
            ImageView icon;
            TextView label;
        }

        public LocationAdapter(Context context, List<ProfileStore.Location> values) {
            super(context, values);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ProfileStore.Location location = getItem(position);

            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = Manager.getLayoutInflater(getActivity()).inflate(R.layout.layout_square_grid_item, parent, false);
                holder.icon = (ImageView) convertView.findViewById(R.id.icon);
                holder.label = (TextView) convertView.findViewById(R.id.label);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            String resource = location.resource;
            String label = location.label;

            int drawableId = getResources().getIdentifier(resource, "drawable", getContext().getPackageName());

            holder.label.setText(label);
            holder.icon.setBackgroundResource(drawableId);

            convertView.setLayoutParams(new AbsListView.LayoutParams(getContainerWidth(), getContainerHeight()));
            convertView.setBackgroundColor(getResources().getColor(R.color.background));

            return convertView;
        }
    }

    @SuppressLint("ValidFragment")
    public class ActionOverlayDialogFragment extends DialogFragment {

        private int position;

        public ActionOverlayDialogFragment(View view, int position) {
            this.position = position;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_overal_add_contact, container, false);
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog dialog = super.onCreateDialog(savedInstanceState);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            return dialog;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }
    }
}
