package com.digitalmisfits.pigeon.app.activities.child.preferences;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.digitalmisfits.android.view.PagingLinearLayout;
import com.digitalmisfits.pigeon.app.R;

public class PreferencesActivity extends FragmentActivity {

    public static final String TAG = "PreferencesActivity";

    private static final int FRAGMENTS = 4;

    private PreferenceFragmentPagerAdapter adapter;
    private ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);

        adapter = new PreferenceFragmentPagerAdapter(getSupportFragmentManager());

        pager = (ViewPager) findViewById(R.id.viewPager);

        PagingLinearLayout navigation = (PagingLinearLayout) findViewById(R.id.navigation);
        navigation.setViewPager(pager);

        // add the adapter to the gridview
        adapter = new PreferenceFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setCurrentItem(1);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "onDestroy()");
    }


    public class PreferenceFragmentPagerAdapter extends FragmentPagerAdapter {

        public PreferenceFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return FRAGMENTS;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return new PreferenceLocationFragment();
                }
                case 2: {
                    return new PreferenceApplicationFragment();
                }
                case 3: {
                    return new PreferenceAdvancedFragment();
                }
                default: {
                    return new PreferenceContactFragment();
                }
            }
        }
    }
}
