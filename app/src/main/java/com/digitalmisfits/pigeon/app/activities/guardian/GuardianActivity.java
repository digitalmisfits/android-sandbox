package com.digitalmisfits.pigeon.app.activities.guardian;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalmisfits.android.common.Manager;
import com.digitalmisfits.pigeon.app.R;
import com.digitalmisfits.pigeon.app.activities.child.fragment.AbstractContainerFragment;
import com.digitalmisfits.pigeon.app.activities.guardian.dialog.TokenDialog;
import com.digitalmisfits.pigeon.client.DefaultPigeonClient;
import com.digitalmisfits.pigeon.model.Profile;
import com.digitalmisfits.pigeon.store.ProfileStore;

import java.util.ArrayList;
import java.util.List;


public class GuardianActivity extends ActionBarActivity implements TokenDialog.TokenDialogListener {

    public static final String TAG = "GuardianActivity";

    MainFragment main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guardian);

        main = new MainFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, main);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.guardian, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_add_child) {

            TokenDialog dialog = new TokenDialog();
            dialog.show(getSupportFragmentManager(), "Link");
            return true;

        } else if (id == R.id.action_show_map) {

            startActivity(new Intent(this, GuardianMaps.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTokenDialogOkClick(String token) {

        (new AsyncProfileLinkTask(token)).execute();
    }

    public class AsyncProfileLinkTask extends AsyncTask<Void, Boolean, Boolean> {

        private ProgressDialog dialog;

        private DefaultPigeonClient client = DefaultPigeonClient.getInstance();

        private String token;

        public AsyncProfileLinkTask(String token) {
            this.token = token;
        }

        protected void onPreExecute() {

            dialog = new ProgressDialog(GuardianActivity.this);
            dialog.setTitle("Pigeon");
            dialog.setMessage("Linking devices...");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.show();
        }

        protected Boolean doInBackground(Void... params) {

            return client.link(ProfileStore.getUUID(), token);
        }

        protected void onPostExecute(Boolean result) {

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            main.updateAdapter();
        }
    }

    public static class MainFragment extends AbstractContainerFragment {

        private ChildAdapter adapter;

        public MainFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_viewpager_grid, container, false);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            adapter = new ChildAdapter(getActivity(), new ArrayList<Profile>());
            GridView v = (GridView) getView().findViewById(R.id.gridview);
            v.setAdapter(adapter);

            (new AsyncProfileLoader()).execute();
        }

        @Override
        public int getCalculatedComponentHeight() {
            return getStatusBarHeight() + getActionBarHeight();
        }

        public void updateAdapter() {

            Log.d(TAG, "Updating adapter");

            (new AsyncProfileLoader()).execute();
        }

        public class ChildAdapter extends AbstractGridViewAdapter<Profile> {

            private String[] colors = {
                    "#ff9300",
                    "#6ca437",
                    "#5ec6ff",
                    "#de5d59",
            };

            private class ViewHolder {
                RelativeLayout canvas;
                ImageView icon;
                TextView label;
            }

            public ChildAdapter(Context context, List<Profile> values) {
                super(context, values);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                Profile profile = getItem(position);

                ViewHolder holder;
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = Manager.getLayoutInflater(getActivity()).inflate(R.layout.layout_square_grid_item, parent, false);
                    holder.icon = (ImageView) convertView.findViewById(R.id.icon);
                    holder.label = (TextView) convertView.findViewById(R.id.label);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }


                int drawableId = R.drawable.empty_child;
                String label = "empty".toUpperCase();
                String color = "#FFFFFF";

                if (profile.getUuid() != null) {
                    label = profile.getName();
                    color = colors[position % colors.length];
                    drawableId = R.drawable.child;
                }

                holder.label.setText(label);
                holder.icon.setBackgroundResource(drawableId);

                convertView.setLayoutParams(new AbsListView.LayoutParams(getContainerWidth(), getContainerHeight()));
                convertView.setBackgroundColor(Color.parseColor(color));

                return convertView;
            }
        }

        public class AsyncProfileLoader extends AsyncTask<Void, List<Profile>, List<Profile>> {

            private ProgressDialog dialog;

            private DefaultPigeonClient client = DefaultPigeonClient.getInstance();

            protected void onPreExecute() {

                dialog = new ProgressDialog(MainFragment.this.getActivity());
                dialog.setTitle("Pigeon");
                dialog.setMessage("Refreshing...");
                dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                dialog.show();
            }

            protected List<Profile> doInBackground(Void... params) {

                List<Profile> profiles = client.getChildProfiles(ProfileStore.getUUID());

                if (profiles == null) {
                    profiles = new ArrayList<>();
                }

                Log.d(TAG, String.format("Profiles length: %d", profiles.size()));
                for (Profile p : profiles) {
                    Log.d(TAG, String.format("Profiles uuid: %s", p.getUuid()));
                }

                int treshold = 4 - profiles.size();
                for (int i = 0; i < treshold; i++) {
                    profiles.add(new Profile());
                }

                return profiles;
            }

            protected void onPostExecute(List<Profile> result) {

                adapter.clear();
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                    adapter.addAll(result);
                } else {
                    for (Profile p : result) {
                        adapter.add(p);
                    }
                }

                adapter.notifyDataSetChanged();

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }
    }
}
