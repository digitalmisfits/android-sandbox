package com.digitalmisfits.pigeon.app.activities.guardian;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.digitalmisfits.pigeon.app.R;

public class GuardianMaps extends FragmentActivity {

    public static final String TAG = "GuardianMaps";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guardian_maps);

        FragmentManager fmanager = getSupportFragmentManager();
        Fragment fragment = fmanager.findFragmentById(R.id.map);
        SupportMapFragment supportmapfragment = (SupportMapFragment) fragment;

        GoogleMap map = supportmapfragment.getMap();
        map.setMyLocationEnabled(true);

        if(getIntent().hasExtra("latlng")) {
            double lat = getIntent().getDoubleExtra("lat", 0);
            double lng = getIntent().getDoubleExtra("lng", 0);

            Log.d(TAG, String.format("lat %f, lng %f", lat, lng));

            LatLng location = new LatLng(lat, lng);

            map.addMarker(new MarkerOptions().position(location));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 13));
        }
    }
}
