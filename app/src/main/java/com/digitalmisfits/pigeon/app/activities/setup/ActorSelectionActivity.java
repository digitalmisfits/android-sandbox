package com.digitalmisfits.pigeon.app.activities.setup;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.digitalmisfits.pigeon.app.R;
import com.digitalmisfits.pigeon.app.Settings;
import com.digitalmisfits.pigeon.app.activities.guardian.GuardianActivity;
import com.digitalmisfits.pigeon.client.DefaultPigeonClient;
import com.digitalmisfits.pigeon.model.Profile;
import com.digitalmisfits.pigeon.store.ProfileStore;
import com.digitalmisfits.pigeon.typedef.Actor;

import java.util.UUID;

public class ActorSelectionActivity extends Activity {

    private static final String TAG = "ActorSelectionActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actor_selection);

        LinearLayout guardianBtn = (LinearLayout) findViewById(R.id.guardian_btn);
        LinearLayout childBtn = (LinearLayout) findViewById(R.id.child_btn);

        guardianBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                createProfileForActor(Actor.GUARDIAN);
            }
        });

        childBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                createProfileForActor(Actor.CHILD);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void createProfileForActor(Actor actor) {

        ProfileStore.clear();
        ProfileStore.setName("Dummy");
        ProfileStore.setActor(actor);
        ProfileStore.setProfileId(UUID.randomUUID().toString());

        new AsyncRegistrationTask().execute();
    }

    private class AsyncRegistrationTask extends AsyncTask<Void, Void, Boolean> {

        private DefaultPigeonClient client = DefaultPigeonClient.getInstance();
        private ProgressDialog dialog;

        protected void onPreExecute() {

            dialog = new ProgressDialog(ActorSelectionActivity.this);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setTitle("Pigeon");
            dialog.setMessage("Registering client...");
            dialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            Profile p = new Profile();
            p.setName("Dummy");
            p.setActor(ProfileStore.getActor().name());
            p.setClientId(ProfileStore.getProfileId());
            p.setRegistrationId(Settings.getGcmRegistrationId(ActorSelectionActivity.this));

//            String uuid = client.registerProfile(p);

            ProfileStore.setUUID("1234");
            return true;
        }

        protected void onPostExecute(Boolean result) {

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            if(result) {
                if(ProfileStore.getActor() == Actor.GUARDIAN) {
                    Intent intent = new Intent(ActorSelectionActivity.this, GuardianActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(ActorSelectionActivity.this, LauncherSetupActivity.class);
                    startActivity(intent);
                }
            } else {
                AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(ActorSelectionActivity.this);

                dlgAlert.setMessage("Registration failed. Please check your internet connection.");
                dlgAlert.setTitle("Error");
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.create().show();
            }
        }
    }
}
