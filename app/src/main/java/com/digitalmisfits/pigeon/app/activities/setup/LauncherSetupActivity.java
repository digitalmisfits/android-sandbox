package com.digitalmisfits.pigeon.app.activities.setup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.digitalmisfits.android.common.Launcher;
import com.digitalmisfits.pigeon.app.R;
import com.digitalmisfits.pigeon.app.activities.child.HomeActivity;

public class LauncherSetupActivity extends Activity {

    private static final String TAG = "LauncherSetupActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher_select);

        LinearLayout enableBtn = (LinearLayout) findViewById(R.id.enable_btn);
        enableBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Launcher.setDefaultLauncher(LauncherSetupActivity.this, HomeActivity.class);
            }
        });

        if (getIntent().hasExtra("exit")) {
            Log.d(TAG, "Exit requested");
            return;
        }

        if (!Launcher.isDefaultLauncher(this, getPackageName(), HomeActivity.class)) {
            Log.d(TAG, "Current application is not the default launcher.");
        } else {

            Log.d(TAG, "Current application is the default launcher. Starting Home activity.");

            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            /*
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
            */
            overridePendingTransition(0, 0);
            startActivity(intent);
            finish();
            return;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }
}
