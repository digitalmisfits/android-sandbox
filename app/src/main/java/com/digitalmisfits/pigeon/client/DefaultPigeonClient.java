package com.digitalmisfits.pigeon.client;

import android.util.Log;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.digitalmisfits.pigeon.model.Location;
import com.digitalmisfits.pigeon.model.Profile;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;

import org.jboss.netty.handler.codec.http.HttpResponseStatus;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DefaultPigeonClient {

    public static final String TAG = "DefaultPigeonClient";
    public static final String endpoint = "http://pigeon.digitalmisfits.com/app_dev.php/";

    protected AsyncHttpClient http = new AsyncHttpClient();
    protected Gson gson = new Gson();

    private static DefaultPigeonClient instance = null;

    protected DefaultPigeonClient() {
    }

    public static DefaultPigeonClient getInstance() {

        if (instance == null) {
            instance = new DefaultPigeonClient();
        }

        return instance;
    }

    public String registerProfile(Profile profile) {
        Map<String, Collection<String>> parameters = new HashMap<>();

        parameters.put("name", Lists.newArrayList(profile.getName()));
        parameters.put("actor", Lists.newArrayList(profile.getActor()));
        parameters.put("clientId", Lists.newArrayList(profile.getClientId()));
        parameters.put("registrationId", Lists.newArrayList(profile.getRegistrationId()));

        Response response = post("register", parameters);
        if (response != null) {

            try {
                Log.d(TAG, String.format("response.getResponseBody(): %s", response.getResponseBody()));
            } catch (IOException e) {
                Log.d(TAG, e.getMessage(), e);
            }

            if (response.getStatusCode() == HttpResponseStatus.OK.getCode()) {

                try {
                    return gson.fromJson(response.getResponseBody(), new TypeToken<String>() {
                    }.getType());
                } catch (IOException e) {
                    Log.d(TAG, e.getMessage(), e);
                    return null;
                }
            }
        }

        return null;
    }

    public boolean setRegistrationId(String uuid, String registrationId) {

        Map<String, Collection<String>> parameters = new HashMap<>();

        Response response = post(String.format("%s/messaging/%s", uuid, registrationId), parameters);
        if (response != null) {

            try {
                Log.d(TAG, String.format("response.getResponseBody(): %s", response.getResponseBody()));
            } catch (IOException e) {
                Log.d(TAG, e.getMessage(), e);
            }

            if (response.getStatusCode() == HttpResponseStatus.OK.getCode()) {
                return true;
            }
        }

        return false;
    }

    public String getToken(String uuid) {
        Map<String, Collection<String>> parameters = new HashMap<>();

        Response response = post(String.format("%s/token", uuid), parameters);
        if (response != null) {

            if (response.getStatusCode() == HttpResponseStatus.OK.getCode()) {

                try {
                    return gson.fromJson(response.getResponseBody(), new TypeToken<String>() {
                    }.getType());
                } catch (IOException e) {
                    Log.d(TAG, e.getMessage(), e);
                    return null;
                }
            }
        }

        return null;
    }


    public boolean checkin(String uuid, String name, Location location) {

        Map<String, Collection<String>> parameters = new HashMap<>();

        parameters.put("name", Lists.newArrayList(name));
        parameters.put("lat", Lists.newArrayList(String.valueOf(location.getLat())));
        parameters.put("lng", Lists.newArrayList(String.valueOf(location.getLng())));

        Response response = post(String.format("%s/check-in", uuid), parameters);
        if (response != null) {

            try {
                Log.d(TAG, String.format("response.getResponseBody(): %s", response.getResponseBody()));
            } catch (IOException e) {
                Log.d(TAG, e.getMessage(), e);
            }

            if (response.getStatusCode() == HttpResponseStatus.OK.getCode()) {
                return true;
            }
        }

        return false;
    }

    public List<Profile> getChildProfiles(String uuid) {

        Response response = post(String.format("%s/children", uuid), new HashMap<String, Collection<String>>());
        if (response != null) {

            try {
                Log.d(TAG, String.format("response.getResponseBody(): %s", response.getResponseBody()));
            } catch (IOException e) {
                Log.d(TAG, e.getMessage(), e);
            }

            if (response.getStatusCode() == HttpResponseStatus.OK.getCode()) {

                try {
                    return gson.fromJson(response.getResponseBody(), new TypeToken<List<Profile>>() {
                    }.getType());
                } catch (IOException e) {
                    Log.d(TAG, e.getMessage(), e);
                    return null;
                }
            }
        }

        return null;
    }

    public boolean link(String uuid, String token) {

        HashMap<String, Collection<String>> parameters = new HashMap<>();

        Response response = post(String.format("%s/link/%s", uuid, token), parameters);
        if (response != null) {

            if (response.getStatusCode() == HttpResponseStatus.OK.getCode()) {

                try {
                    Log.d(TAG, String.format("response.getResponseBody(): %s", response.getResponseBody()));
                } catch (IOException e) {
                    Log.d(TAG, e.getMessage(), e);
                }

                try {
                    return gson.fromJson(response.getResponseBody(), new TypeToken<Boolean>() {
                    }.getType());
                } catch (IOException e) {
                    Log.d(TAG, e.getMessage(), e);
                    return false;
                }
            }
        }

        return false;
    }

    public Response put(Map<String, Collection<String>> parameters) {
        return post("", parameters);
    }

    public Response post(String path, Map<String, Collection<String>> parameters) {

        Log.d(TAG, String.format("Requested %s", path));

        AsyncHttpClient.BoundRequestBuilder builder = http.preparePost(endpoint + path);
        builder.setHeader("Content-Type", "application/x-www-form-urlencoded");
        builder.setParameters(parameters);

        try {
            return builder.execute().get();
        } catch (Exception e) {
            Log.d(TAG, e.getMessage(), e);
        }

        return null;
    }
}
