package com.digitalmisfits.pigeon.receiver;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.digitalmisfits.pigeon.service.IdleTimeout;

public class IdleTimeoutReceiver extends WakefulBroadcastReceiver {

    private static final String TAG = "IdleTimeoutReceiver";

    private static boolean isGuardSuspended = false;

    @Override
    public void onReceive(Context context, Intent intent) {

        switch (intent.getAction()) {
            case "com.digitalmisfits.android.IDLE_INTERRUPT": {
                if(isGuardSuspended) {
                    context.sendBroadcast(new Intent("com.digitalmisfits.android.GUARD_RESUME"));
                }
                break;
            }

            case "com.digitalmisfits.android.GUARD_SUSPEND": {
                isGuardSuspended = true;
                break;
            }

            case "com.digitalmisfits.android.GUARD_RESUME": {
                startWakefulService(context, new Intent(context, IdleTimeout.class));
                isGuardSuspended = false;
                break;
            }
        }
    }
}
