package com.digitalmisfits.pigeon.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class PresenceReceiver extends BroadcastReceiver {

    public static final String TAG = "PresenceReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(TAG, "onReceive");

        switch (intent.getAction()) {

            case Intent.ACTION_SCREEN_OFF: {

                context.sendBroadcast(new Intent("com.digitalmisfits.android.GUARD_SUSPEND"));
                break;
            }
            case Intent.ACTION_SCREEN_ON: {

                context.sendBroadcast(new Intent("com.digitalmisfits.android.GUARD_RESUME"));
                break;
            }
        }
    }
}
