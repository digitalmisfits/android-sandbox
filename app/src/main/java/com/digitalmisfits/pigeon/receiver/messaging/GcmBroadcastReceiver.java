package com.digitalmisfits.pigeon.receiver.messaging;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.digitalmisfits.pigeon.service.messaging.GcmIntentService;


public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

    private static final String TAG = "GcmBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        ComponentName comp = new ComponentName(context.getPackageName(), GcmIntentService.class.getName());
        intent.setComponent(comp);

        startWakefulService(context, intent);
        setResultCode(Activity.RESULT_OK);
    }
}