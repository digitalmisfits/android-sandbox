package com.digitalmisfits.pigeon.receiver.messaging;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.digitalmisfits.pigeon.service.messaging.GcmRegistrationService;

public class GcmRegistrationAlarmReceiver extends WakefulBroadcastReceiver {

    private static final String TAG = "GcmRegistrationAlarmReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        Intent service = new Intent(context, GcmRegistrationService.class);
        startWakefulService(context, service);
    }
}
