package com.digitalmisfits.pigeon.receiver.supervisor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.digitalmisfits.pigeon.service.scheduler.SupervisorAlarmScheduler;

public class BootReceiver extends BroadcastReceiver {

    public static final String TAG = "BootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            SupervisorAlarmScheduler.schedule(context);
        }
    }
}
