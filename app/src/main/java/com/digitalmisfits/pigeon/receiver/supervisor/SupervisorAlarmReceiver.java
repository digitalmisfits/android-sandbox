package com.digitalmisfits.pigeon.receiver.supervisor;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.digitalmisfits.pigeon.service.Supervisor;

public class SupervisorAlarmReceiver extends WakefulBroadcastReceiver {

    private static final String TAG = "SupervisorAlarmReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        Intent service = new Intent(context, Supervisor.class);
        startWakefulService(context, service);
    }
}
