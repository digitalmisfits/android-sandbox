package com.digitalmisfits.pigeon.receiver.telephony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import static android.provider.Telephony.Sms.Intents.SMS_RECEIVED_ACTION;

public class GenericSMSReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(SMS_RECEIVED_ACTION)) {
            abortBroadcast();
        }
    }
}

