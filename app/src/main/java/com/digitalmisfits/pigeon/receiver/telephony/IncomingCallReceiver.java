package com.digitalmisfits.pigeon.receiver.telephony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.digitalmisfits.pigeon.store.ProfileStore;

import java.lang.reflect.Method;

public class IncomingCallReceiver extends BroadcastReceiver {

    public final static String TAG = "IncomingCallReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        if (!intent.getAction().equals("android.intent.action.PHONE_STATE"))
            return;

        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {

            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

            Log.i(TAG, "Incoming call received from " + incomingNumber);

            boolean isAllowed = false;
            for (ProfileStore.Contact contact : ProfileStore.getContacts()) {
                if (contact.phone != null && contact.phone.equals(incomingNumber)) {
                    isAllowed = true;
                    break;
                }
            }

            if (isAllowed || incomingNumber == null)
                return;

            Log.i(TAG, "Blocking incoming call from " + incomingNumber);

            AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            audio.setStreamMute(AudioManager.STREAM_RING, true);
            audio.setRingerMode(AudioManager.RINGER_MODE_SILENT);

            TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            try {

                Class cls = Class.forName(telephony.getClass().getName());
                Method method = cls.getDeclaredMethod("getITelephony");
                method.setAccessible(true);

                ITelephony telephonyService = (ITelephony) method.invoke(telephony);
                telephonyService = (ITelephony) method.invoke(telephony);
                telephonyService.endCall();

                if (telephonyService.isIdle()) {
                    Log.d(TAG, "Successfully ended call");
                }

            } catch (Exception e) {
                Log.d(TAG, e.getMessage(), e);
            } finally {
                audio.setStreamMute(AudioManager.STREAM_RING, false);
                audio.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
            }
        }

        Log.d(TAG, String.format("Exiting %s", getClass().getName()));
    }
}