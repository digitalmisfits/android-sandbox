package com.digitalmisfits.pigeon.receiver.telephony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.digitalmisfits.pigeon.store.ProfileStore;

public class OutgoingCallReceiver extends BroadcastReceiver {

    private static final String TAG = "OutgoingCallReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

            boolean isAllowed = false;

            for(ProfileStore.Contact contact: ProfileStore.getContacts()) {
                if(contact.phone != null && contact.phone.equals(number)) {
                    isAllowed = true;
                    break;
                }
            }

             if(!isAllowed) {
                Log.d(TAG, String.format("Blocked outgoing call to %s", number));
                setResultData(null);
            }
        }
    }
}
