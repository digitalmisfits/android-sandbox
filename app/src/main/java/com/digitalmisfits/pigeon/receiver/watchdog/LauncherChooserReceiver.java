package com.digitalmisfits.pigeon.receiver.watchdog;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

public class LauncherChooserReceiver extends WakefulBroadcastReceiver {

    private static final String TAG = "LauncherChooserReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(TAG, String.format("Action %s", intent.getAction() != null ? intent.getAction() : "None"));
    }
}
