package com.digitalmisfits.pigeon.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.digitalmisfits.android.common.Component;
import com.digitalmisfits.android.common.Manager;
import com.digitalmisfits.android.common.fsm.EnumState;
import com.digitalmisfits.android.common.fsm.FiniteStateMachine;
import com.digitalmisfits.pigeon.app.R;
import com.digitalmisfits.pigeon.app.Settings;
import com.digitalmisfits.pigeon.app.activities.child.HomeActivity;
import com.digitalmisfits.pigeon.receiver.guard.GuardOperationReceiver;
import com.digitalmisfits.pigeon.receiver.telephony.GenericSMSReceiver;
import com.digitalmisfits.pigeon.receiver.telephony.IncomingCallReceiver;
import com.digitalmisfits.pigeon.receiver.telephony.OutgoingCallReceiver;
import com.digitalmisfits.pigeon.service.scheduler.SupervisorAlarmScheduler;
import com.digitalmisfits.pigeon.store.ProfileStore;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

public class Guard extends Service {

    private static final String TAG = "Guard";

    /**
     * The GuardLifecycleState defines the possible states this service can be in and transition to.
     */
    public enum GuardLifecycleState implements EnumState<GuardLifecycleState> {

        INIT("INITIALISE") {
            public Set<GuardLifecycleState> possibleFollowUps() {
                return EnumSet.of(START, KILL);
            }
        },

        START("START") {
            public Set<GuardLifecycleState> possibleFollowUps() {
                return EnumSet.of(STOP, KILL);
            }
        },

        STOP("STOP") {
            public Set<GuardLifecycleState> possibleFollowUps() {
                return EnumSet.of(START, KILL);
            }
        },

        KILL("KILL") {
            public Set<GuardLifecycleState> possibleFollowUps() {
                return EnumSet.noneOf(GuardLifecycleState.class);
            }
        };

        private final String action;

        GuardLifecycleState(String action) {
            this.action = action;
        }

        public String getAction() {
            return this.action;
        }

        public static GuardLifecycleState getLifecycleState(String action) {
            for (GuardLifecycleState state : GuardLifecycleState.values()) {
                if (state.getAction().equals(action)) {
                    return state;
                }
            }

            return null;
        }

        @Override
        public String toString() {
            return getAction();
        }
    }

    FiniteStateMachine<GuardLifecycleState> fsm;

    /* receivers */
    private Class<?>[] receivers = {
            GenericSMSReceiver.class,
            IncomingCallReceiver.class,
            OutgoingCallReceiver.class,
            GuardOperationReceiver.class
    };

    private Handler handler = null;
    private GuardThreadRunnable runnable = null;

    @Override
    public void onCreate() {
        super.onCreate();

        handler = new Handler();
        runnable = new GuardThreadRunnable(this);
        fsm = new FiniteStateMachine<>();

        fsm.addPreTransitionListener(new FiniteStateMachine.StateTransitionListener<GuardLifecycleState>() {

            @Override
            public void execute(GuardLifecycleState state, GuardLifecycleState previous, Intent intent) {

                Log.d(TAG, String.format("State: %s", state.getAction()));

                handler.removeCallbacksAndMessages(null);
            }
        });

        fsm.addPostTransitionListener(new FiniteStateMachine.StateTransitionListener<GuardLifecycleState>() {

            @Override
            public void execute(GuardLifecycleState state, GuardLifecycleState previous, Intent intent) {
                Intent stateIntent = new Intent("com.digitalmisfits.pigeon.guard.LIFECYCLE_STATE");
                stateIntent.putExtra("state", state.getAction());
                sendBroadcast(stateIntent);
            }
        });

        fsm.addStateTransitionListener(GuardLifecycleState.INIT, new FiniteStateMachine.StateTransitionListener<GuardLifecycleState>() {
            @Override
            public void execute(GuardLifecycleState state, GuardLifecycleState previous, Intent intent) {
                SupervisorAlarmScheduler.schedule(getApplicationContext());
                Component.enableComponentSetting(getApplicationContext(), receivers);
            }
        });

        fsm.addStateTransitionListener(GuardLifecycleState.START, new FiniteStateMachine.StateTransitionListener<GuardLifecycleState>() {
            @Override
            public void execute(GuardLifecycleState state, GuardLifecycleState previous, Intent intent) {
                handler.postDelayed(runnable, 150L);
            }
        });

        fsm.addStateTransitionListener(GuardLifecycleState.STOP, new FiniteStateMachine.StateTransitionListener<GuardLifecycleState>() {
            @Override
            public void execute(GuardLifecycleState state, GuardLifecycleState previous, Intent intent) {
                // default PreTransitionListener
            }
        });

        fsm.addStateTransitionListener(GuardLifecycleState.KILL, new FiniteStateMachine.StateTransitionListener<GuardLifecycleState>() {
            @Override
            public void execute(GuardLifecycleState state, GuardLifecycleState previous, Intent intent) {
                SupervisorAlarmScheduler.unschedule(getApplicationContext());

                Log.d(TAG, "Disabling receivers");
                Component.resetComponentSetting(getApplicationContext(), receivers);

                if (intent != null && intent.getBooleanExtra("disableLauncherComponent", false)) {
                    Component.resetComponentSetting(getApplicationContext(), HomeActivity.class);
                }

                stopSelf();
            }
        });

        startForeground(0x10, getForegroundNotification());
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(android.content.Intent intent, int flags, int startId) {

        if (intent.getAction() != null) {
            fsm.doTransition(GuardLifecycleState.getLifecycleState(intent.getAction()), intent);
        }

        return START_STICKY;
    }


    /**
     * @return a foreground notification with pending HomeActivity intent
     */
    private Notification getForegroundNotification() {

        Notification notification;

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {

            notification = new Notification(R.drawable.shield, "Pigeon", System.currentTimeMillis());
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                    new Intent(this, HomeActivity.class), 0);

            notification.setLatestEventInfo(this, "Pigeon", "Guard enabled", contentIntent);

        } else {

            NotificationCompat.Builder notificationBuider = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.shield)
                    .setContentTitle("Pigeon")
                    .setContentText("Guard enabled");

            notification = notificationBuider.build();
        }

        return notification;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        fsm.doTransition(GuardLifecycleState.KILL, null);
    }

    private class GuardThreadRunnable implements Runnable {

        private static final String TAG = "GuardThreadRunnable";

        private final Context context;
        private final ActivityManager activity;
        private final ITelephony telephony;

        public GuardThreadRunnable(Context ctx) {

            context = ctx;
            activity = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
            telephony = Manager.getTelephonyService(context);
        }

        /**
         * Determines if there is a currently active call (OFFHOOK||RINGING)
         * <p/>
         * Needs refactoring to a embedded broadcast receiver to minimize the cpu (and thus battery) usage of
         * continuously polling the telephony status
         *
         * @return
         */
        public boolean isCallActive() {
            int callState = Manager.getTelephonyManager(getApplicationContext()).getCallState();

            if (callState == TelephonyManager.CALL_STATE_OFFHOOK || callState == TelephonyManager.CALL_STATE_RINGING) {
                return true;
            }

            return false;
        }

        /**
         * Broadcast ACTION_CLOSE_SYSTEM_DIALOGS to close any open overlay (notification bar, recent apps menu, etc.)
         * <p/>
         * Different implementations exists (HTC Sense, TouchWiz) which in some cases close an open keyboard or have other
         * non-standard behaviour. Consult the Android source for the three different "reasons"; "globalactions" tend to work
         * best across all different implementations
         */
        public void broadcastCloseSystemDialogs() {

            Intent intent = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            //intent.putExtra("reason", "globalactions");
            sendBroadcast(intent);
        }

        public void closeStatusBar() {

            int currentApiVersion = android.os.Build.VERSION.SDK_INT;

            try {
                Object service = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");

                if (service != null) {
                    if (currentApiVersion <= Build.VERSION_CODES.JELLY_BEAN) {
                        Method collapse = statusbarManager.getMethod("collapse");
                        collapse.setAccessible(true);
                        collapse.invoke(service);
                    } else {
                        Method collapse2 = statusbarManager.getMethod("collapsePanels");
                        collapse2.setAccessible(true);
                        collapse2.invoke(service);
                    }
                }

            } catch (Exception e) {
                Log.d(TAG, e.getMessage(), e);
            }
        }

        /**
         * Move a specific task to the front of the activity stack
         *
         * @param taskInfo
         */
        public void moveTaskToFront(ActivityManager.RunningTaskInfo taskInfo) {

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                activity.moveTaskToFront(taskInfo.id, ActivityManager.MOVE_TASK_WITH_HOME);

            } else {

                Intent intent = new Intent();
                intent.setComponent(taskInfo.topActivity);
                intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                try {
                    context.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Log.d("TAG", e.getMessage(), e);
                }

            }
        }

        @Override
        public void run() {

            closeStatusBar();

            List<ActivityManager.RunningTaskInfo> runningTopTasks = activity.getRunningTasks(1);
            ActivityManager.RunningTaskInfo topTask = runningTopTasks.get(0);

            if (!runningTopTasks.isEmpty()) {

                if (isCallActive()) {
                    if (!topTask.topActivity.getPackageName().equals("com.android.dialer")) {
                        if (telephony instanceof ITelephony) {
                            try {
                                telephony.showCallScreen();
                            } catch (RemoteException e) {
                                Log.d(TAG, "Unable to showCallScreen");
                            }
                        }

                        handler.postDelayed(this, 150L);
                        return;
                    }
                }

                ArrayList<String> applications = ProfileStore.getApplications();
                applications.addAll(Settings.getWhiteListedSystemPackages());

                if (!applications.contains(topTask.topActivity.getPackageName())) {

                    Log.d(TAG, String.format("Blocking %s / %s ", topTask.topActivity.getPackageName(), topTask.topActivity.getClassName()));

                    ActivityManager.RunningTaskInfo info = null;

                    /* look up task identifier for the current package */
                    List<ActivityManager.RunningTaskInfo> runningTasks = activity.getRunningTasks(16);

                    for (ActivityManager.RunningTaskInfo runningTask : runningTasks) {
                        if (runningTask.topActivity.getPackageName().equals(context.getPackageName())) {
                            info = runningTask;
                            break;
                        }
                    }

                    if (info != null)
                        moveTaskToFront(info);
                }
            }

            handler.postDelayed(this, 150L);
        }
    }
}
