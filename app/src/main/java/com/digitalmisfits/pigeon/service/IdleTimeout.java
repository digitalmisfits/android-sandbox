package com.digitalmisfits.pigeon.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.WakefulBroadcastReceiver;


public class IdleTimeout extends Service {

    public static final String INTERRUPTED = "Interrupted";
    private static final String TAG = "IdleTimeout";
    private Handler handler = null;
    private TimeoutRunnable runnable = null;

    @Override
    public void onCreate() {
        handler = new Handler();
        runnable = new TimeoutRunnable();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(android.content.Intent intent, int flags, int startId) {

        rescheduleTimeoutRunnable();

        /* release wake lock */
        WakefulBroadcastReceiver.completeWakefulIntent(intent);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }

    /**
     * Stop current runnable and re-schedule
     */
    private void rescheduleTimeoutRunnable() {

        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(runnable, 5000L);
    }

    private class TimeoutRunnable implements Runnable {

        public void run() {

            //sendBroadcast(new Intent("com.digitalmisfits.android.GUARD_SUSPEND"));
        }
    }

    private class PreemtiveTimeoutRunnable implements Runnable {

        public void run() {

            sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
        }
    }
}
