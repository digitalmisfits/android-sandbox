package com.digitalmisfits.pigeon.service;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import java.util.List;

public class Supervisor extends IntentService {

    private static final String TAG = "Supervisor";

    public Supervisor() {
        super("Supervisor");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        final ActivityManager activity = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        if (activity == null) {
            return;
        }

        List<RunningTaskInfo> runningTasks = activity.getRunningTasks(128);

        boolean isRunning = false;

        for (RunningTaskInfo task : runningTasks) {
            String pkg = task.topActivity.getPackageName();
            String cls = task.topActivity.getClassName();

            if (pkg.equals(getPackageName()) || pkg.equals("android") && cls.equals("com.android.internal.app.ResolverActivity")) {
                isRunning = true;
            }
        }

        if (!isRunning) {
            Log.d(TAG, "Restarting application...");

            Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
        }

        WakefulBroadcastReceiver.completeWakefulIntent(intent);
    }
}
