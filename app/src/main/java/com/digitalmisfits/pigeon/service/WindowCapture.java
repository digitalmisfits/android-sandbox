package com.digitalmisfits.pigeon.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.digitalmisfits.android.view.StickyOverlayViewGroup;

public class WindowCapture extends Service {

    private static final String TAG = "WindowCapture";

    private StickyOverlayViewGroup stickyOverlay;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        stickyOverlay = new StickyOverlayViewGroup(this);

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                1, 1,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSPARENT);

        params.gravity = Gravity.RIGHT | Gravity.TOP;

        WindowCaptureEventListener eventListener = new WindowCaptureEventListener();

        stickyOverlay.setOnTouchListener(eventListener);
        stickyOverlay.setOnKeyListener(eventListener);

        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.addView(stickyOverlay, params);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "WindowCapture destroyed");

        if (stickyOverlay != null) {
            ((WindowManager) getSystemService(WINDOW_SERVICE)).removeView(stickyOverlay);
        }

        stickyOverlay = null;
    }

    private static class WindowCaptureEventListener implements View.OnTouchListener, View.OnKeyListener {

        public boolean onKey(View v, int keyCode, KeyEvent event) {

            return false;
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {

            Context c = view.getContext().getApplicationContext();

            if (motionEvent.getAction() == MotionEvent.ACTION_OUTSIDE) {
                Log.d(TAG, "MotionEvent.ACTION_OUTSIDE");
                // c.sendBroadcast(new Intent("com.digitalmisfits.android.IDLE_INTERRUPT"));
            }

            return false;
        }
    }
}

