package com.digitalmisfits.pigeon.service.messaging;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.digitalmisfits.pigeon.app.R;
import com.digitalmisfits.pigeon.app.activities.guardian.GuardianMaps;
import com.digitalmisfits.pigeon.model.CheckIn;
import com.digitalmisfits.pigeon.receiver.messaging.GcmBroadcastReceiver;
import com.digitalmisfits.pigeon.store.ProfileStore;
import com.digitalmisfits.pigeon.typedef.Actor;

public class GcmIntentService extends IntentService {

    private static final String TAG = "GcmIntentService";

    private static final int NOTIFICATION_ID = 0x01;

    private static final Gson gson = new Gson();

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.d(TAG, "onHandleIntent");

        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {

            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                // error
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                // deleted messages on the server
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

                Log.d(TAG, String.format("Extras: %s", extras.toString()));

                if (ProfileStore.getActor() == Actor.GUARDIAN) {

                    CheckIn checkIn = null;

                    try {
                        checkIn = gson.fromJson(extras.getString("message"), new TypeToken<CheckIn>() {
                        }.getType());
                    } catch (JsonSyntaxException e) {
                        Log.d(TAG, e.getMessage(), e);
                    }

                    if (checkIn != null) {
                        displayNotification(checkIn.getLocation().getLat(), checkIn.getLocation().getLng());
                    }
                }
            }
        }

        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    public void displayNotification(double lat, double lng) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle("Check-in")
                        .setContentText("Dummy checked in at ....");

        Log.d(TAG, String.format("lat %f, lng %f", lat, lng));

        Intent resultIntent = new Intent(this, GuardianMaps.class);
        resultIntent.putExtra("latlng", true);
        resultIntent.putExtra("lat", lat);
        resultIntent.putExtra("lng", lng);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(GuardianMaps.class);
        stackBuilder.addNextIntent(resultIntent);

        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                (int) System.currentTimeMillis(), PendingIntent.FLAG_UPDATE_CURRENT
        );

        mBuilder.setAutoCancel(true);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}