package com.digitalmisfits.pigeon.service.messaging;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.digitalmisfits.pigeon.client.DefaultPigeonClient;
import com.digitalmisfits.pigeon.app.Settings;
import com.digitalmisfits.pigeon.receiver.messaging.GcmBroadcastReceiver;
import com.digitalmisfits.pigeon.service.scheduler.GcmRegistrationAlarmScheduler;
import com.digitalmisfits.pigeon.store.ProfileStore;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This service re-schedules itself using exponential back-off if registration failed
 */
public class GcmRegistrationService extends IntentService {

    public static final String TAG = "GcmRegistrationService";

    public static final AtomicInteger attempts = new AtomicInteger();

    public GcmRegistrationService() {
        super("GcmRegistrationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Context context = this.getApplicationContext();
        GoogleCloudMessaging messaging = GoogleCloudMessaging.getInstance(context);

        try {
            if (Settings.getGcmRegistrationId(context).isEmpty()) {
                try {
                    String registrationId = messaging.register(Settings.GCM_SENDER_ID);

                    if (!"".equals(registrationId)) {
                        Settings.setGcmRegistrationId(context, registrationId);

                        String uuid = ProfileStore.getUUID();

                        if(uuid != null && !"".equals(uuid)) {
                            DefaultPigeonClient.getInstance().setRegistrationId(uuid, registrationId);
                        }

                        attempts.set(0); // reset attempts

                    } else {
                        rescheduleRegistrationAlarm(context);
                    }

                } catch (IOException e) {
                    Log.d(TAG, e.getMessage(), e);
                    rescheduleRegistrationAlarm(context);
                }
            }
        } finally {
            GcmBroadcastReceiver.completeWakefulIntent(intent);
        }

        Log.d(TAG, String.format("GoogleCloudMessaging registration id %s", Settings.getGcmRegistrationId(context)));
    }

    private void rescheduleRegistrationAlarm(Context context) {
        Log.d(TAG, "Rescheduling GoogleCloudMessaging registration alarm");

        GcmRegistrationAlarmScheduler.schedule(context, attempts.incrementAndGet(), Integer.MAX_VALUE);
    }
}