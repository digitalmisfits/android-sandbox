package com.digitalmisfits.pigeon.service.scheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.digitalmisfits.pigeon.receiver.messaging.GcmRegistrationAlarmReceiver;

/**
 * GoogleCloudMessaging registration scheduler with exponential back-off
 */
public class GcmRegistrationAlarmScheduler {

    public static final String TAG = "GcmRegistrationAlarmScheduler";

    /* trigger in seconds */
    private static final int TRIGGER_AT_MILLIS = 3000;

    public static void schedule(Context context, int attempt, int maxAttemps) {

        if (isPreviouslyScheduled(context, GcmRegistrationAlarmReceiver.class)) {
            return;
        }

        if(attempt > maxAttemps) {
            Log.d(TAG, String.format("Maximum number of attempts reached: %d", maxAttemps));
            return;
        }

        long backOff = Math.round(Math.pow((double) TRIGGER_AT_MILLIS, (double) attempt));
        Log.d(TAG, String.format("exponential backOff set to %d", backOff));

        Intent i = new Intent(context, GcmRegistrationAlarmReceiver.class);
        PendingIntent p = PendingIntent.getBroadcast(context, 0, i, 0);

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.ELAPSED_REALTIME, backOff, p);
    }

    public static boolean isPreviouslyScheduled(Context context, java.lang.Class<?> cls) {

        Intent i = new Intent(context, cls);

        if(PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_NO_CREATE) != null) {
            return true;
        }
        return false;
    }

    public static void unschedule(Context context) {

        Intent i = new Intent(context, GcmRegistrationAlarmReceiver.class);

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.cancel(PendingIntent.getBroadcast(context, 0, i, 0));
    }

}
