package com.digitalmisfits.pigeon.service.scheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.digitalmisfits.pigeon.receiver.supervisor.SupervisorAlarmReceiver;

public class SupervisorAlarmScheduler {

    public static final String TAG = "SupervisorAlarmScheduler";

    public static final int INTERVAL = 5000;

    public static void schedule(Context context) {

        if (isPreviouslyScheduled(context, SupervisorAlarmReceiver.class)) {
            return;
        }

        Intent i = new Intent(context, SupervisorAlarmReceiver.class);
        PendingIntent p = PendingIntent.getBroadcast(context, 0, i, 0);

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, INTERVAL, INTERVAL, p);
    }

    public static void unschedule(Context context) {

        Intent i = new Intent(context, SupervisorAlarmReceiver.class);

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.cancel(PendingIntent.getBroadcast(context, 0, i, 0));
    }

    public static boolean isPreviouslyScheduled(Context context, java.lang.Class<?> cls) {

        Intent i = new Intent(context, cls);

        if(PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_NO_CREATE) != null) {
            return true;
        }

        return false;
    }
}
