package com.digitalmisfits.pigeon.store;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.digitalmisfits.pigeon.app.Pigeon;
import com.digitalmisfits.pigeon.app.Settings;
import com.digitalmisfits.pigeon.typedef.Actor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ProfileStore {

    private static final String TAG = "ProfileStore";

    private static final Gson gson = new Gson();

    public static Actor getActor() {
        return Actor.lookup(getSharedPreferences().getInt("actor", Integer.MIN_VALUE));
    }

    public static void setActor(Actor actor) {
        getSharedPreferences().edit().putInt("actor", actor.getId()).commit();
    }

    public static String getName() {
        String name = getSharedPreferences().getString("name", "");

        if (name.isEmpty())
            return null;

        return name;
    }

    public static void setName(String name) {
        getSharedPreferences().edit().putString("name", name).commit();
    }

    public static String getProfileId() {
        String profileId = getSharedPreferences().getString("profile-id", "");

        if (profileId.isEmpty())
            return null;

        return profileId;
    }

    public static void setProfileId(String profileId) {

        getSharedPreferences().edit().putString("profile-id", profileId).commit();
    }

    public static void setUUID(String uuid) {
        getSharedPreferences().edit().putString("uuid", uuid).commit();
    }

    public static String getUUID() {
        String profileId = getSharedPreferences().getString("uuid", "");

        if (profileId.isEmpty())
            return null;

        return profileId;
    }

    public static ArrayList<String> getApplications() {

        ArrayList<String> applications;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            applications = Lists.newArrayList(getSharedPreferences().getStringSet("applications", new HashSet<String>()));
        } else {

            String serialized = getSharedPreferences().getString("applications", "");

            if ("".equals(serialized)) {
                applications = Lists.newArrayList();
            } else {
                applications = gson.fromJson(serialized, new TypeToken<ArrayList<String>>() {}.getType());
            }
        }

        if (applications.isEmpty() || !applications.contains("com.google.android.apps.maps"))
            applications.add("com.google.android.apps.maps");

        return applications;
    }

    public static void setApplications(List<String> applications) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            getSharedPreferences().edit().putStringSet("applications", Sets.newHashSet(applications)).commit();
        } else {
            getSharedPreferences().edit().putString("applications", gson.toJson(applications)).commit();
        }
    }

    public static List<Location> getLocations() {

        String json = Settings.getJsonFromAsset("locations.json");

        List<Map<String, String>> literals = gson.fromJson(json,
                new TypeToken<List<Map<String, String>>>() {
                }.getType());

        List<Location> locations = new LinkedList<>();

        for (Map<String, String> location : literals) {

            Location l = new Location();
            l.label = location.get("label");
            l.color = location.get("color");
            l.resource = location.get("resource");
            l.lat = Double.parseDouble(location.get("lat"));
            l.lng = Double.parseDouble(location.get("lng"));

            locations.add(l);
        }

        return locations;
    }

    public static void setLocations(LinkedList<Location> locations) {

        if (!locations.isEmpty()) {
            setCollection("locations", locations);
        }
    }

    public static LinkedList<Contact> getContacts() {

        String json = getSharedPreferences().getString("contacts", "");

        LinkedList<Contact> collection = new LinkedList<>();

        try {
            collection = gson.fromJson(json, new TypeToken<LinkedList<Contact>>() {
            }.getType());
        } catch (Exception e) {
            Log.d(TAG, e.getMessage(), e);
        }

        if (collection == null) {
            collection = new LinkedList<>();
        }

        int treshold = 4 - collection.size();

        for (int i = 0; i < treshold; i++) {
            collection.add(new Contact(true));
        }

        return collection;
    }

    public static void setContacts(LinkedList<Contact> collection) {

        if (collection.isEmpty())
            return;

        String json = null;

        try {
            json = gson.toJson(collection, new TypeToken<LinkedList<Contact>>() {
            }.getType());
        } catch (Exception e) {
            Log.d(TAG, e.getMessage(), e);
        }

        if (!(json != null && json.isEmpty())) {
            getSharedPreferences().edit().putString("contacts", json).commit();
        }
    }

    public static void clear() {
        getSharedPreferences().edit().clear().commit();
    }

    public static SharedPreferences getSharedPreferences() {
        return Pigeon.getCustomApplicationContext().getSharedPreferences(ProfileStore.class.getName(), Context.MODE_PRIVATE);
    }

    private static <T> LinkedList<T> getCollection(String key) {

        String json = getSharedPreferences().getString(key, "");

        Log.d(TAG, json);

        if (json.isEmpty()) {
            return new LinkedList<T>();
        }

        LinkedList<T> collection = null;

        try {
            collection = gson.fromJson(json, new TypeToken<LinkedList<T>>() {
            }.getType());
        } catch (Exception e) {
            Log.d(TAG, e.getMessage(), e);
        }

        return collection;
    }

    private static <T> void setCollection(String key, LinkedList<T> collection) {

        String json = null;

        try {
            json = gson.toJson(collection, new TypeToken<LinkedList<T>>() {
            }.getType());
        } catch (Exception e) {
            Log.d(TAG, e.getMessage(), e);
        }

        Log.d(TAG, json);

        if (!(json != null && json.isEmpty())) {
            getSharedPreferences().edit().putString(key, json).commit();
        }
    }

    public static Map<String, String> getColorMap() {

        Map<String, String> colorMap = new HashMap<>();

        colorMap.put("mummy", "#d881ae");
        colorMap.put("daddy", "#1aae72");
        colorMap.put("granny", "#b0b0b0");
        colorMap.put("patrick", "#fdc100");
        colorMap.put("school", "#ff9300");
        colorMap.put("soccer", "#6ca437");
        colorMap.put("swimming", "#5ec6ff");
        colorMap.put("music", "#de5d59");

        return colorMap;
    }

    public static class Location implements Serializable {

        private static final long serialVersionUID = 7526471155622776147L;

        public String label;
        public String resource;
        public String color;

        public boolean isPlaceholder = false;

        public double lat;
        public double lng;

        public Location() {

        }

        public Location(boolean isPlaceholder) {
            this.isPlaceholder = isPlaceholder;
        }
    }

    public static class Contact implements Serializable {

        private static final long serialVersionUID = 7526471155622776148L;

        public String label;
        public String resource;
        public String color;
        public String phone;

        public boolean isPlaceholder = false;

        public Contact() {

        }

        public Contact(boolean isPlaceholder) {
            this.isPlaceholder = isPlaceholder;
        }
    }

    public static class Application implements Serializable {

        private static final long serialVersionUID = 7526471155622776149L;

        public boolean isPlaceholder = false;

        public String packageName;
        public String className;
    }
}