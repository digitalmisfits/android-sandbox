package com.digitalmisfits.pigeon.typedef;


public enum Actor {
    GUARDIAN(0x01), CHILD(0x02);

    private final int id;

    Actor(int n) {
        this.id = n;
    }

    public int getId() {
        return id;
    }

    public static Actor lookup(int id) {
        for (Actor actor : Actor.values()) {
            if (actor.getId() == id) {
                return actor;
            }
        }

        return null;
    }
}
