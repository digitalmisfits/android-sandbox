package com.digitalmisfits.pigeon.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class PigeonTextView extends TextView {

    public PigeonTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public PigeonTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PigeonTextView(Context context) {
        super(context);
        init();
    }

    private void init() {

        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/futura.ttf");
            setTypeface(tf);
        }
    }
}