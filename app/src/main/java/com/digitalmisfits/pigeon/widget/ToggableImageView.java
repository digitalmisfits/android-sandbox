package com.digitalmisfits.pigeon.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.digitalmisfits.pigeon.app.R;

public class ToggableImageView extends ImageView {

    private Drawable drawableActive;
    private Drawable drawableInactive;

    public ToggableImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public ToggableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ToggableImageView(Context context) {
        super(context);
    }

    public void init(AttributeSet attrs) {

        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.ToggableImageView);

        final int n = a.getIndexCount();
        for (int i = 0; i < n; ++i) {

            int attr = a.getIndex(i);

            switch (attr) {
                case R.styleable.ToggableImageView_drawableActive:
                    setDrawableActive(a.getDrawable(attr));
                    break;
                case R.styleable.ToggableImageView_drawableInactive:
                    setDrawableInactive(a.getDrawable(attr));
                    break;
            }
        }

        a.recycle();
    }

    public void setActive(boolean isActive) {

        if (isActive) {
            setImageDrawable(getDrawableActive());
            setBackgroundColor(getResources().getColor(R.color.navigation_btn_selected));
        } else {
            setImageDrawable(getDrawableInactive());
            setBackgroundColor(getResources().getColor(R.color.black));
        }
    }


    public Drawable getDrawableActive() {
        return drawableActive;
    }

    public void setDrawableActive(Drawable drawableActive) {
        this.drawableActive = drawableActive;
    }

    public Drawable getDrawableInactive() {
        return drawableInactive;
    }

    public void setDrawableInactive(Drawable drawableInactive) {
        this.drawableInactive = drawableInactive;
    }
}
